import React from 'react';
import {createRoot} from 'react-dom/client'
import {BrowserRouter, Route, Routes} from "react-router-dom";
import Broker from "./components/broker/Broker";
import GM from "./components/gm/GM";
import AgentStats from "./components/gm/page/AgentStats";
import {Chooser} from "./components/Chooser";
import {TelexRoot} from "./components/telex/TelexRoot";
import {AgentStatsOverviewRoot} from "./components/agentStatsOverview/AgentStatsOverviewRoot";

// const store = initStore();
// initWebSocket(store);
// initClock(store);

// render(
//     <Provider store={store}>
//         <Chooser/>
//     </Provider>,
//     document.getElementById('root')
// );

// fetch("/api/state")
//     .then(response =>
//         response.json().then(data =>
//             store.dispatch({type: "INIT_STATE", state: data})
//         )
//     );


const container = document.getElementById('app') as HTMLDivElement
const root = createRoot(container)


root.render(
    <div className="container-fluid">
        <BrowserRouter>
            <Routes>
                <Route path="/broker" element={<Broker/>}/>
                <Route path="/gm" element={<GM/>}/>
                <Route path="/telex" element={<TelexRoot/>}/>
                <Route path="/agentStats" element={<AgentStatsOverviewRoot/>}/>
                <Route path="*" element={<Chooser/>}/>
            </Routes>
        </BrowserRouter>
    </div>
)
