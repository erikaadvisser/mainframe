import iziToast from "izitoast";

export const iziError = (text: string) => {
    iziToast.show({
        title: "Error",
        message: text,
        position: 'topCenter',
        color: "red",
        timeout: 5000,
    })
}


interface IziMessageProps {
    text: string
    title?: string
    color?: string
}

export const iziMessage = ( {title, text, color}: IziMessageProps) => {
    iziToast.show({
        title: title,
        message: text,
        position: 'topCenter',
        color: color
    })
}

export const iziQuick = (text: string) => {
    iziToast.show({
        message: text,
        position: 'topCenter',
        color: "yellow",
        timeout: 1500,
        progressBar: false,
    })
}
