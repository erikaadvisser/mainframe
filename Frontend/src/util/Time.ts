export const nowString = () => {
    let now = new Date()
    let minutes = now.getMinutes()
    let hours = now.getHours()
    let minuteString = (minutes > 0) ? minutes : "0" + minutes
    let hourString = (hours > 0) ? hours : "0" + hours
    return hourString + ":" + minuteString
}

