import {Store} from "redux";
import webstomp, {Client, Frame, Message} from 'webstomp-client'
import iziToast from "izitoast";
import {error} from "./ErrorUtils";
import {iziQuick} from "./Izi";

export class StompConnection {

    client: Client = null as unknown as Client
    store: Store = null as unknown as Store
    url = "ws://localhost:80/ws"

    connect(store: Store, onConnect: () => void) {
        this.client = webstomp.client(this.url)
        this.client.connect({},
            (event: Frame | undefined) => {
                this.onOpen(event)
                onConnect()
            },
            this.onError)
    }

    onOpen(event: Frame | undefined) {
        if (event == undefined) {
            error("Connection with Mainframe failed to establish")
        }
        iziQuick('Connection with Mainframe established')

        // this.client.subscribe('/topic/broker', this.reduxWsEvent)

        // fetch("/api/income/getTimer")
        // fetch("/api/auction/getTimer")
    }

    subscribe(path: string) {
        this.client.subscribe('/topic/broker', this.processEvent)
    }


    onError(event: CloseEvent | Frame) {
        iziToast.show({
            title: 'Fatal',
            message: 'Connection with mainframe lost. Please refresh.',
            position: 'topCenter',
            color: 'red',
            timeout: false
        })
    }

    processEvent(stompMessage: Message) {
        const body = JSON.parse(stompMessage.body)
        const message = body.message

        if (message && this.store.getState().user !== "Telex") {
            iziToast.show({
                title: message.title,
                message: message.text,
                position: 'topCenter',
                color: message.color,
                timeout: message.timeout

            })
        }
        let event = {...message, globalState: this.store.getState()}
        this.store.dispatch(event)
    }
}

export const stompConnection = new StompConnection()