import React, {Component} from 'react';
import {connect} from 'react-redux'
import {agents} from '../../../constants/Agents'


const emptyState = () => {
    return {
        agentId: -1,
    };
};

class AgentStatsUI extends Component {

    constructor(props) {
        super(props);
        this.state = emptyState();
    }

    render() {

        const selectAgent = agentId => {
            let stats = this.props.agents.find(agent => agent.id === agentId).stats;
            this.setState({
                agentId: agentId,
            });
        };

        const changeStat = (stat, delta) => {
            if (this.state.agentId === -1) { return }

            let message = { id: this.state.agentId, stat: stat, delta: delta };
            fetch("/api/agent/stats", {
                method: 'post',
                body: JSON.stringify(message),
                headers: {
                    'Content-Type': 'application/json'
                },
            })
        };

        const statValue = (stat) => {
            if (this.state.agentId === -1) { return "" }
            return this.props.agents.find( agent => agent.id === this.state.agentId ).stats[stat];
        }

        return (
            <span>
            <div className="row">
                <div className="col-lg-12">
                    <h2>
                        <span className="label label-info"
                              onClick={() => this.props.selectPage("home")}>Home</span>&nbsp;
                        <span className="label label-default">Agent stats</span>
                    </h2>
                </div>
            </div>
            <div className="row">
                <div className="col-lg-12">
                    <hr className="hr"/>
                </div>
            </div>
            <div className="row">
                <div className="col-lg-3">
                    {agents.map(agent =>
                        <div onClick={() => selectAgent(agent.id)}>
                            <span className={'btn buttonRow ' + agent.color}>O</span>
                            <span
                                className={'btn buttonRow button btn-default' + (this.state.agentId === agent.id ? ' active' : '')}>{agent.name}</span>
                        </div>
                    )}
                </div>
                    <div className="col-md-4">

                <div className="row">
                    <div className="col-md-6 xfield">Infiltration</div>

                    <div className="input-group input-group-lg">
                        <span className="input-group-addon" id="infiltration_min" onClick={ () => changeStat("INFILTRATION", -1)}>-</span>
                        <input type="text" name="infiltration" value={statValue("INFILTRATION")} className="form-control"
                               id="infiltration"/>
                        <span className="input-group-addon" id="infiltration_plus"  onClick={ () => changeStat("INFILTRATION", 1)}>+</span>
                    </div>
                    <br/>
                </div>
                <div className="row">
                    <div className="col-md-6 xfield">Persuasion</div>

                    <div className="input-group input-group-lg">
                        <span className="input-group-addon" id="persuasion_min" onClick={ () => changeStat("PERSUASION", -1)}>-</span>
                        <input type="text" name="persuasion" value={statValue("PERSUASION")} className="form-control"
                               id="persuasion"/>
                        <span className="input-group-addon" id="persuasion_plus" onClick={ () => changeStat("PERSUASION", 1)}>+</span>
                    </div>
                </div>
                <br/>
                        <div className="row">
                            <div className="col-md-6 xfield">Science</div>

                            <div className="input-group input-group-lg">
                                <span className="input-group-addon" id="science_min" onClick={ () => changeStat("SCIENCE", -1)}>-</span>
                                <input type="text" name="science" value={statValue("SCIENCE")} className="form-control" id="science"/>
                                <span className="input-group-addon" id="science_plus" onClick={ () => changeStat("SCIENCE", 1)}>+</span>
                            </div>
                        </div>
                        <br/>
                        <div className="row">
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            <input type="hidden" name="_fired" value=""/>
                            <input type="hidden" name="_hiding" value=""/>

                            <div className="btn-group-vertical btn-group-lg" data-toggle="buttons">
                                <label className="btn btn-default active">
                                    <input type="checkbox" name="fired" value="true" checked="true"></input> Fired
                                </label>
                            </div>

                            <div className="btn-group-vertical btn-group-lg" data-toggle="buttons">
                                <label className="btn btn-default">
                                    <input type="checkbox" name="hiding" value="true"></input>Hiding
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
            </span>
        )
    }
}

const mapStateToProps = state => {
        return {
            agents: state.agents
        }
    }
;

const mapDispatchToProps = dispatch => {
        return {
            selectPage: name => {
                dispatch({type: "SELECT_PAGE", page: name});
            },
            dispatch
        }
    }
;

const AgentStats = connect(mapStateToProps, mapDispatchToProps)(AgentStatsUI);
export default AgentStats