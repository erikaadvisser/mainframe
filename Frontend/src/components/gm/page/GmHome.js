import React from 'react';
import { connect } from 'react-redux'

const GmHomeUI = ({ selectPage }) => {

    return (
        <span>
            <div className="row">
                <div className="col-lg-12">
                    <h2>
                        <span className="label label-default">Home</span>
                    </h2>
                </div>
            </div>
            <div className="row">
                <div className="col-lg-12">
                    <hr className="hr"/>
                </div>
            </div>
            <div className="row">
                <div className="col-lg-4">
                    <div onClick={() => selectPage("complete_story_mission")}>
                        <span className='btn btn-info buttonRow gmButton'>Complete story mission</span>
                    </div>
                    <div onClick={() => selectPage("process_story_evidence")}>
                        <span className='btn btn-info buttonRow gmButton'>Process story evidence</span>
                    </div>
                    <div onClick={() => selectPage("x")}>
                        <span className='btn btn-info buttonRow gmButton disabled' >Process local mission</span>
                    </div>
                </div>
                <div className="col-lg-4">
                    <div onClick={() => selectPage("x")}>
                        <span className='btn btn-info buttonRow gmButton disabled'>Process raid</span>
                    </div>
                    <div onClick={() => selectPage("x")}>
                        <span className='btn btn-info buttonRow gmButton disabled'>Cover up</span>
                    </div>
                    <div onClick={() => selectPage("x")}>
                        <span className='btn btn-info buttonRow gmButton disabled'>Agents cause panic</span>
                    </div>
                </div>
                <div className="col-lg-4">
                    <div onClick={() => selectPage("agent_stats")}>
                        <span className='btn btn-info buttonRow gmButton'>Agent stats</span>
                    </div>
                    <div onClick={() => selectPage("x")}>
                        <span className='btn btn-info buttonRow gmButton disabled'>Turn order</span>
                    </div>
                </div>
            </div>
        </span>
    )
};

const mapStateToProps = state => {
    return {
    }
};

const mapDispatchToProps = dispatch => {
    return {
        selectPage: name => { dispatch({type: "SELECT_PAGE", page: name}); },
    }
};

const GmHome = connect(
    mapStateToProps,
    mapDispatchToProps
)(GmHomeUI);

export default GmHome
