import React, {Component} from 'react';
import {connect} from 'react-redux'
import { agents } from '../../../constants/Agents'
import {iziError, iziMessage} from "../../../util/Izi";

const emptyState = () => {
    let agentsSelected = { 0: false, 1: false, 2: false, 3: false, 4: false, 5: false};
    return {missionSelected: null, agentsSelected: agentsSelected};
};

class CompleteStoryMissionUI extends Component {

    constructor(props) {
        super(props);
        this.state = emptyState()
    }

    render = () => {
        let selectMission = id => {
            this.setState({"missionSelected": id});
        };

        let selectAgent = agentId => {
            let state = this.state.agentsSelected;
            state[agentId]= !state[agentId];
            this.setState({"agentsSelected": state});
        };

        let selectOutcome = (resolution) => {
            let agentIds = [];
            Object.keys(this.state.agentsSelected).forEach( (key) => {
                if (this.state.agentsSelected[key]) {
                    agentIds.push(parseInt(key, 10));
                }
            });

            if (this.state.missionSelected === null) {
                iziError("Select mission");
                return;
            }
            if (agentIds.length === 0) {
                iziError("Select agent(s)");
                return;
            }

            let message = { id: this.state.missionSelected, agentIds: agentIds };
            fetch("/api/mission/" + resolution, {
                method: 'post',
                body: JSON.stringify(message),
                headers: {
                    'Content-Type': 'application/json'
                },
            })
            .then(response =>
                response.json().then(json => {
                    this.setState(emptyState());
                    iziMessage(json.message);
                    if (json.success) {
                        this.props.dispatch({type: "MISSION_UPDATE", missions: json.data});
                    }
                })
            );

        };

        selectMission = selectMission.bind(this);
        selectOutcome = selectOutcome.bind(this);


        return (
            <span>
                <div className="row">
                    <div className="col-lg-12">
                        <h2>
                            <span className="label label-info" onClick={() => this.props.selectPage("home")}>Home</span>&nbsp;
                            <span className="label label-default">Complete Story Mission</span>
                        </h2>
                    </div>
                </div>
                <div className="row">
                    <div className="col-lg-12">
                        <hr className="hr"/>
                    </div>
                </div>
                <div className="row">
                    <div className="col-lg-3">
                        {this.props.missions
                            .filter(mission => mission.state === "ACTIVE")
                            .map(mission =>
                            <div onClick={() => selectMission(mission.id)}>
                                <span className={ 'btn btn-default buttonRow button' + (this.state.missionSelected === mission.id ? ' active': '') } >{mission.name}</span>
                            </div>
                        )}
                    </div>
                    <div className="col-lg-3">
                        {agents.map(agent =>
                            <div onClick={() => selectAgent(agent.id)}>
                                <span className={'btn buttonRow button btn-default' + (this.state.agentsSelected[agent.id] ? ' active': '')}>{agent.name}</span>
                                <span className={'btn buttonRow ' + agent.color }>O</span>
                                </div>
                        )}
                    </div>
                    <div className="col-lg-4">
                        <div onClick={() => selectOutcome("success")}>
                            <span className='btn btn-info buttonRow gmButton'>Success</span>
                        </div>
                        <div onClick={() => selectOutcome("failure")}>
                            <span className='btn btn-info buttonRow gmButton'>Failure</span>
                        </div>
                        <div onClick={() => selectOutcome("expose")}>
                            <span className='btn btn-info buttonRow gmButton'>Expose</span>
                        </div>
                        <div onClick={() => selectOutcome("expose_foiled")}>
                            <span className='btn btn-info buttonRow gmButton'>Expose foiled</span>
                        </div>
                    </div>
                </div>
            </span>
        )
    }

};

const mapStateToProps = state => {
    return {
        missions: state.missions,
    }
};

const mapDispatchToProps = dispatch => {
    return {
        selectPage: name => { dispatch({type: "SELECT_PAGE", page: name}); },
        dispatch
    }
};

const CompleteStoryMission = connect(mapStateToProps, mapDispatchToProps)(CompleteStoryMissionUI);
export default CompleteStoryMission
