import React, {Component} from 'react';
import {connect} from 'react-redux'
import {agents} from '../../../constants/Agents'
import {iziError, iziMessage} from "../../../util/Izi";

const emptyState = () => ({
    missionSelected: null,
    agentSelected: null,
    smokerSelected: null
});

class ProcessStoryEvidenceUI extends Component {

    constructor(props) {
        super(props);
        this.state = emptyState();
    }

    render = () => {
        let selectMission = id => {
            this.setState({"missionSelected": id});
        };

        let selectAgent = agentId => {
            this.setState({"agentSelected": agentId});
        };

        let selectSmoker = smokerId => {
            this.setState({"smokerSelected": smokerId});
        };

        let selectOutcome = (resolution) => {
            if (this.state.missionSelected === null) {
                iziError("Select mission");
                return;
            }
            if (this.state.agentSelected === null) {
                iziError("Select agent(s)");
                return;
            }
            if (resolution === "embezzle" && this.state.smokerSelected === null) {
                iziError("Select a smoker for embezzle");
                return;
            }

            let message = {
                id: this.state.missionSelected,
                agentIds: [this.state.agentSelected],
                smokerId: this.state.smokerSelected,
            };

            fetch("/api/mission/" + resolution, {
                method: 'post',
                body: JSON.stringify(message),
                headers: {
                    'Content-Type': 'application/json'
                },
            }).then(response =>
                response.json().then(json => {
                        this.setState(emptyState());
                        iziMessage(json.message);
                        if (json.success) {
                            this.props.dispatch({type: "MISSION_UPDATE", missions: json.data});
                        }
                    }
                )
            );
        };

        selectMission = selectMission.bind(this);
        selectOutcome = selectOutcome.bind(this);
        selectSmoker = selectSmoker.bind(this);

        return (
            <span>
                <div className="row">
                    <div className="col-lg-12">
                        <h2>
                            <span className="label label-info"
                                  onClick={() => this.props.selectPage("home")}>Home</span>&nbsp;
                            <span className="label label-default">Complete Story Mission</span>
                        </h2>
                    </div>
                </div>
                <div className="row">
                    <div className="col-lg-12">
                        <hr className="hr"/>
                    </div>
                </div>
                <div className="row">
                    <div className="col-lg-3">
                        {this.props.missions
                            .filter(mission => mission.state === "PENDING")
                            .map(mission =>
                                <div onClick={() => selectMission(mission.id)}>
                                    <span
                                        className={'btn btn-default buttonRow button' + (this.state.missionSelected === mission.id ? ' active' : '')}>{mission.name}</span>
                                </div>
                            )}
                    </div>
                    <div className="col-lg-3">
                        {agents.map(agent =>
                            <div onClick={() => selectAgent(agent.id)}>
                                <span
                                    className={'btn buttonRow button btn-default' + (this.state.agentSelected === agent.id ? ' active' : '')}>{agent.name}</span>
                                <span className={'btn buttonRow ' + agent.color}>O</span>
                            </div>
                        )}
                    </div>
                    <div className="col-lg-3">
                        {this.props.smokers
                            .filter(smoker => smoker.smoker)
                            .map((smoker) =>
                                <div onClick={() => selectSmoker(smoker.id)}>
                                    <span
                                        className={'btn btn-default buttonRow button' + (this.state.smokerSelected === smoker.id ? ' active' : '')}>{smoker.name}</span>
                                </div>
                            )
                        }

                    </div>
                    <div className="col-lg-3">
                        <div onClick={() => selectOutcome("file")}>
                            <span className='btn btn-info buttonRow gmButton'>File</span>
                        </div>
                        <div onClick={() => selectOutcome("embezzle")}>
                            <span className='btn btn-info buttonRow gmButton'>Embezzle</span>
                        </div>
                        <div onClick={() => selectOutcome("expose")}>
                            <span className='btn btn-info buttonRow gmButton'>Expose</span>
                        </div>
                        <div onClick={() => selectOutcome("Fail")}>
                            <span className='btn btn-info buttonRow gmButton'>Fail</span>
                        </div>
                    </div>
                </div>
            </span>
        )
    }

}

const mapStateToProps = state => {
    return {
        missions: state.missions,
        smokers: state.smokers
    }
};

const mapDispatchToProps = dispatch => {
    return {
        selectPage: name => {
            dispatch({type: "SELECT_PAGE", page: name});
        },
        dispatch
    }
};

const ProcessStoryEvidence = connect(mapStateToProps, mapDispatchToProps)(ProcessStoryEvidenceUI);
export default ProcessStoryEvidence
