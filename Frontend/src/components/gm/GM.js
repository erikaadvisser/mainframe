import React from 'react';
import Notifications from '../generic/Notifications.js';
import {connect} from "react-redux";
import NotificationSender from "../generic/NotificationSender";
import GmHome from "./page/GmHome";
import CompleteStoryMission from "./page/CompleteStoryMission";
import ProcessStoryEvidence from "./page/ProcessStoryEvidence";
import AgentStats from "./page/AgentStats";


const gmPage = (page) => {
    switch (page) {
        case "home"                   : return <GmHome />;
        case "complete_story_mission" : return <CompleteStoryMission />;
        case "process_story_evidence" : return <ProcessStoryEvidence />;
        case "agent_stats"            : return <AgentStats />

        case "loading"                : return "Loading...";
        default                       : return <div>error, unknown page "{page}".</div>
    }

};

const GmUI = ({page}) => {
    return (
        <span>
            <div className="row">
                <div className="col-lg-6">
                    <div className="col-md-3" style={{"paddingLeft": 0}}>
                        <div className="panel panel-default">
                            <div className="panel-heading" style={{"backgroundColor": "#428bca", "color": "white"}}>
                                <strong >FBI<br />Dana Scully</strong>
                            </div>

                            <div id="turnTime" className="panel-body">00:00:12</div>
                        </div>
                    </div>

                    { gmPage(page) }
                </div>
                <div className="col-lg-6">
                    {/*<GmHeader />*/}
                    <Notifications />
                </div>
            </div>
            <div className="row">
                <div className="col-lg-12">
                    <NotificationSender />
                </div>
            </div>
        </span>
    )
};

const mapStateToProps = state => {
    return {
        page: state.page
    }
};

const GM = connect(mapStateToProps)(GmUI);
export default GM
