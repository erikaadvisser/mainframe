import React from 'react';
import Notifications from '../generic/Notifications.js';
import {connect} from "react-redux";
import IncomeTimer from "./IncomeTimer";
import NotificationSender from "../generic/NotificationSender";
import Transaction from "./page/Transaction";
import Smoker from "./page/Smoker";
import BrokerHome from "./page/BrokerHome";
import Project from "./page/Projects";
import AuctionTimer from "./AuctionTimer";

const selectPage = (page) => {

    switch (page) {
        case "loading"      : return <div>Loading</div>;
        case "home"         : return <BrokerHome />;
        case "smoker"       : return <Smoker />;
        case "transaction"  : return <Transaction />;
        case "project"      : return <Project />
        case "telex"        : return <span />;

        default             : return <div>error, unknown page "{page}".</div>
    }
};

const BrokerUi = ({page}) => {
    return (
        <span>
            <div className="row">
                <div className="col-lg-6">
                    { selectPage(page) }
                </div>
                <div className="col-lg-6">
                    <div className="row">
                        <div className="col-lg-6">
                            <AuctionTimer />
                        </div>
                        <div className="col-lg-6">
                            <IncomeTimer />
                        </div>
                    </div>
                    <Notifications />
                </div>
            </div>
            <div className="row">
                <div className="col-lg-12">
                    <NotificationSender />
                </div>
            </div>
        </span>
    )
};

const mapStateToProps = state => {
    return {
        page: state.page
    }
};
const Broker = connect(mapStateToProps)(BrokerUi);
export default Broker
