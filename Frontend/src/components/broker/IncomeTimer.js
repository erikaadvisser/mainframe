import React from 'react';
import {connect} from "react-redux";

import TimerUI from "./generic/TimerUI"

const IncomeTimerUi = ({incomeTimer, clickIncome, clickPause, clickPlay}) => {

    return (
        <span>
            <div className="row">
                <div className="col-lg-12">
                    <h1 className="title">
                        <span className="label label-default pull-right">Next income</span>
                    </h1>
                </div>
            </div>
            <div className="row">
                <div className="col-lg-12">
                    <h2 className="pull-right">
                        { TimerUI( {timer: incomeTimer, clickForward: clickIncome, clickPause, clickPlay, clickShowBids: null, clickEnd: null} ) }
                    </h2>
                </div>
            </div>
            <div className="row">
                <div className="col-lg-12">
                    <hr className="hr"/>
                </div>
            </div>
        </span>
    );
};

const mapStateToProps = state => {
    return {
        incomeTimer: state.incomeTimer,
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        clickIncome: () => { fetch("/api/income/do"); },
        clickPause: () => { fetch("/api/income/pause"); },
        clickPlay: () => { fetch("/api/income/play"); },

    }
};

const IncomeTimer = connect(mapStateToProps, mapDispatchToProps)(IncomeTimerUi);
export default IncomeTimer

