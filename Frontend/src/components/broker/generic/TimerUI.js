import React from 'react';

const timeLeft = (timer) => {

    if (!timer.initialized) {
        return (
            <span className="label label-danger">--:--</span>
        );
    }

    let millisDiff = timer.nextMillis - timer.currentMillis;
    let secondsDiff = Math.floor(millisDiff / 1000);
    let secondsAbs = Math.abs(secondsDiff);
    let minutes = Math.floor(secondsAbs / 60);
    let seconds = secondsAbs - (60 * minutes);

    let minutesString = (minutes < 10) ? "0" + minutes : minutes;
    let secondsString = (seconds < 10) ? "0" + seconds : seconds;
    let sign = secondsDiff > 0 ? "" : "-";

    let className = timer.paused ? "label label-warning" : "label label-info";
    return (
        <span className={className} >{sign}{minutesString}:{secondsString}</span>
    );
};

const button = (method, icon) => {
    if (!method) {
        return <span></span>
    }
    const iconClassName = "glyphicon "+ icon;
    return <span className="label label-info" onClick={() => method()}><span  style={{"top": "4px"}} className={iconClassName}/></span>
}


const TimerUI = ({timer, clickShowBids, clickPause, clickPlay, clickEnd, clickForward}) => {

    let pauzePlayClass = timer.paused ? "glyphicon glyphicon-play" : "glyphicon glyphicon-pause";
    let clickPausePlay = timer.paused ? clickPlay: clickPause;

    return (
        <span>
            <span className="label label-info" onClick={() => clickPausePlay()}><span className={pauzePlayClass}/></span>&nbsp;
            {timeLeft(timer)}&nbsp;
            {button(clickForward, "glyphicon-fast-forward")}&nbsp;
            {button(clickShowBids, "glyphicon-info-sign")}&nbsp;
            {button(clickEnd, "glyphicon-ok-sign")}&nbsp;
        </span>
    );
};

export default TimerUI
export {timeLeft}