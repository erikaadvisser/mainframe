import React from 'react';
import {connect} from "react-redux";
import {iziMessage} from "../../../util/Izi";


const ProjectUI = ({missions, startProject}) => {


    const displayIfNotSolved = (missionId, name) => {
        let mission = missions.find(mission => mission.id === missionId);
        if (mission.state === "INACTIVE") {
            return (
                <span className="btn btn-info" onClick={() => startProject(missionId)}>{name} </span>

            );
        }
    };

    return (
        <span>
            <div className="row">
                <div className="col-lg-12">
                    <hr className="hr"/>
                </div>
            </div>
            <div className="row">
                <div className="col-lg-12">
                    <div className="buttonRow">
                        Complete project
                    </div>
                    {displayIfNotSolved(6, "Italian 1")}&nbsp;
                    {displayIfNotSolved(7, "Wong 1")}&nbsp;
                    {displayIfNotSolved(8, "Scientist 1")}&nbsp;
                    {displayIfNotSolved(9, "Dimitri (smoker)")}&nbsp;
                    {displayIfNotSolved(10, "Technocrat (smoker)")}&nbsp;
                </div>
            </div>
        </span>
    )
};

const startProject = (id, dispatch) => {
    fetch("/api/mission/startProject/" + id)
        .then(response =>
            response.json().then(json => {
                iziMessage(json.message);
                dispatch({type: "MISSION_UPDATE", missions: [json.data]});
            })
        );
};

const mapStateToProps = state => {
    return {
        smoker: state.smokers.find((smoker) => {
            return smoker.id === state.smokerId
        }),
        missions: state.missions,
    }
};

const mapDispatchToProps = dispatch => {
    return {
        selectPage: name => {
            dispatch({type: "SELECT_PAGE", page: name});
        },
        startProject: (id) => startProject(id, dispatch)
    }
};

const Project = connect(mapStateToProps, mapDispatchToProps)(ProjectUI);
export default Project