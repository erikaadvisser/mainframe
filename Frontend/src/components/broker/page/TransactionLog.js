import React from 'react';
import {connect} from "react-redux";

const TransactionLogUI = ( {smoker, smokers, transactions} ) => {

    return (
        <div className="row transactions">
            <div className="col-lg-12">
                <table className="table table-striped" id="table">
                    <thead>
                        <tr>
                            <th>Time</th>
                            <th>Amount</th>
                            <th>&nbsp;&nbsp;&nbsp;&nbsp;Who</th>
                            <th>Description</th>
                        </tr>
                    </thead>
                    <tbody>
                    {transactions
                        .filter(transaction => transaction.from === smoker.id || transaction.to === smoker.id)
                        .map(transaction => ( mapTransaction( transaction, smoker, smokers ) ))}
                    </tbody>
                </table>
            </div>
        </div>

    );
};

let mapTransaction = (transaction, smoker, smokers) => {

    let direction;
    let amount = transaction.amount;
    if (transaction.from === smoker.id) {
        direction = "-> " + smokers[transaction.to].name;
        amount = 0 - amount;
    }
    else {
        direction = "<- " + smokers[transaction.from].name;
    }

    return (
        <tr key={transaction.id}>
            <td className="transaction-log-small">{transaction.timeString}</td>
            <td className="transaction-log-small transaction-log-amount">{amount} grip</td>
            <td className="transaction-log-medium">&nbsp;&nbsp;&nbsp;&nbsp;{direction}</td>
            <td>{transaction.description}</td>
        </tr>
    );
};

const mapStateToProps = state => {
    return {
        smoker: state.smokers[state.smokerId],
        smokers: state.smokers,
        transactions: state.transactions,
    }
};

const TransactionLog = connect( mapStateToProps )(TransactionLogUI);
export default TransactionLog