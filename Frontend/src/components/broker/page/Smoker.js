import React from 'react';
import {connect} from "react-redux";
import Mainframe from "../../generic/MainframeTitle";

const SmokerUI = ( { smoker, clickTo }) => {

    return (
        <span>
            <Mainframe />
            <div className="row">
                <div className="col-lg-8">
                    <h2>
                        <span className="label label-info" onClick={() => clickTo("home")}>Main</span>&nbsp;
                        <span className="label label-default">{smoker.name}</span>
                    </h2>
                </div>
                    <div className="col-lg-4">
                        <h2 className="pull-right">
                            <span className="label btn-primary">{smoker.funds}</span>&nbsp;grip
                        </h2>
                    </div>

            </div>
            <div className="row">
                <div className="col-lg-12">
                    <hr className="hr"/>
                </div>
            </div>
            <div className="row">
                <div className="col-lg-12">
                    <div className="buttonRow">
                        <span className="btn btn-info button" onClick={() => clickTo("transaction")}>Transaction</span>
                    </div>
                    <div className="buttonRow">
                        <span className="btn btn-info button" onClick={() => clickTo("project")}>Project</span>
                    </div>
                    <div className="buttonRow">
                        <span className="btn btn-info button disabled">Mission</span>
                    </div>
                    <div className="buttonRow">
                        <span className="btn btn-default button disabled">Auction bid</span>
                    </div>
                </div>
            </div>
        </span>
    );
};

const mapStateToProps = state => {
    return {
        smoker: state.smokers.find( it => it.id === state.smokerId )
    }
};

const mapDispatchToProps = dispatch => {
    return {
        clickTo: (page) => { dispatch({type: "SELECT_PAGE", page: page}); }
    }
};

const Smoker = connect( mapStateToProps, mapDispatchToProps )(SmokerUI);
export default Smoker