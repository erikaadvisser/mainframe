import React, {Component} from 'react';
import {connect} from "react-redux";
import TransactionLog from "./TransactionLog"

import BROKER_ID, {WORLD_ID} from "../../../Constants"
import MainframeTitle from "../../generic/MainframeTitle";

let iziToast = require("izitoast");

class TransactionUI extends Component {


    constructor(props) {
        super(props);
        this.state = {amount: 0};
        this.handleChange = this.handleChange.bind(this);
    }

    add(amount) {
        let oldAmount = parseFloat(this.state.amount);
        if (isNaN(oldAmount)) {
            oldAmount = 0;
        }
        this.setState({amount: oldAmount + amount});
    }

    reset() {
        this.setState({amount: 0});
    }

    handleChange(event) {
        let value = event.target.value;
        this.setState({amount: value});
    }

    render() {
        console.log("render transaction: " + this.props.smoker.name);

        return (
            <span>
                <MainframeTitle />
                <div className="row">
                    <div className="col-lg-8">
                        <h2>
                            <span className="label label-info" onClick={() => this.props.clickTo("home")}>Main</span>&nbsp;
                            <span className="label label-default">{this.props.smoker.name}</span>&nbsp;
                        </h2>
                    </div>
                    <div className="col-lg-4">
                        <h2 className="pull-right">
                            <span className="label btn-primary">{this.props.smoker.funds}</span>&nbsp;grip
                        </h2>
                    </div>
                </div>
                <div className="row">
                    <div className="col-lg-12">
                        <hr className="hr"/>
                    </div>
                </div>
                <div className="row">
                    <div className="col-lg-3">
                        <div className="buttonRow amount-input-row">
                            Amount: <input type="text" className="input-sm amount-input" value={this.state.amount} onChange={this.handleChange}/> grip
                        </div>
                        <div className="buttonRow">
                            <a className="btn btn-info button" onClick={() => this.add(0.5)}>0.5</a>
                        </div>
                        <div className="buttonRow">
                            <a className="btn btn-info button" onClick={() => this.add(1)}> 1</a>
                        </div>
                        <div className="buttonRow">
                            <a className="btn btn-info button" onClick={() => this.add(2)}> 2</a>
                        </div>
                        <div className="buttonRow">
                            <a className="btn btn-info button" onClick={() => this.add(3)}> 3</a>
                        </div>
                        <div className="buttonRow">
                            <span className="btn button" >&nbsp;</span>
                        </div>
                        <div className="buttonRow">
                            <a className="btn btn-info button" onClick={() => this.add(-1)}> -1</a>
                        </div>
                    </div>
                    <div className="col-lg-2">
                        <div className="buttonRow">
                            Action
                        </div>
                        <div className="buttonRow">
                            <span className="btn btn-warning button" onClick={() => { this.props.reward(this.props.smoker, this.state.amount); this.reset();}}>Reward</span>
                        </div>
                        <div className="buttonRow">
                            <span className="btn btn-warning button" onClick={() => { this.props.punish(this.props.smoker, this.state.amount); this.reset();}}>Punishment</span>
                        </div>
                        <div className="buttonRow">
                            <span className="btn btn-warning button" onClick={() => { this.props.auction(this.props.smoker, this.state.amount); this.reset();}}>Auction</span>
                        </div>
                        <div className="buttonRow">
                            <span className="btn btn-warning button" onClick={() => { this.props.missionHandle(this.props.smoker, this.state.amount); this.reset();}}>Handle</span>
                        </div>
                        <div className="buttonRow">
                            <span className="btn btn-success button" onClick={() => { this.props.correctPlus(this.props.smoker, this.state.amount); this.reset();}}>Correction +</span>
                        </div>
                        <div className="buttonRow">
                            <span className="btn btn-warning button" onClick={() => { this.props.correctMinus(this.props.smoker, this.state.amount); this.reset();}}>Correction -</span>
                        </div>
                        <div className="buttonRow">
                            <a className="btn btn-info button" onClick={() => this.props.undo(this.props.smoker)}>Undo</a>
                        </div>
                    </div>
                    <div className="col-lg-2">
                        <div className="buttonRow">
                            Cheque from
                        </div>
                        <div className="buttonRow">
                            { this.props.smokers
                                .filter(smoker => smoker.smoker )
                                .map(smoker =>
                                    <div key={smoker.id} onClick={() => { this.props.chequeFrom(smoker, this.props.smoker, this.state.amount); this.reset();}} >
                                        <span className={'btn btn-success buttonRow button' + ((this.props.smoker === smoker) ? ' disabled': '')}>{smoker.name}</span>
                                    </div>
                                ) }
                        </div>
                    </div>
                    <div className="col-lg-2">
                        <div className="buttonRow">
                            Info on
                        </div>
                        <div className="buttonRow">
                            { this.props.smokers
                                .filter(smoker => smoker.smoker )
                                .map(smoker =>
                                <div key={smoker.id} onClick={() => { this.props.infoOnSmoker(this.props.smoker, smoker, this.state.amount); this.reset();}} >
                                    <span className={'btn btn-warning buttonRow button' + ((this.props.smoker === smoker) ? ' disabled': '')}>{smoker.name}</span>
                                </div>
                            ) }
                        </div>
                    </div>
                    <div className="col-lg-2">
                        <div className="buttonRow">
                            Warfare
                        </div>
                        <div className="buttonRow">
                            { this.props.smokers
                                .filter(smoker => smoker.smoker )
                                .map(smoker =>
                                <div key={smoker.id} onClick={() => { this.props.warfare(this.props.smoker, smoker, this.state.amount); this.reset();}} disabled={smoker === this.props.smoker}>
                                    <span className={'btn btn-danger buttonRow button' + ((this.props.smoker === smoker) ? ' disabled': '')}>{smoker.name}</span>
                                </div>
                            ) }
                        </div>
                    </div>
                </div>
                <TransactionLog smoker={this.props.smoker} smokers={this.props.smokers} />
            </span>
        );
    }
}

let transfer = (action, fromId, toId, amount, description, context, dispatch) => {
    if (fromId === toId) {
        return;
    }
    let amountNumber = parseFloat(amount);
    if (isNaN(amountNumber)) {
        return;
    }

    fetch("/api/fin/transfer/" + action + "/" + fromId + "/" + toId + "/" + amountNumber + "/" + description + "/" + context)
        .then(response =>
            response.json().then(json => {
                    iziToast.show({
                        title: json.message.title,
                        message: json.message.text,
                        position: 'topCenter',
                        color: json.message.color
                    });
                    if (json.success) {
                        dispatch({type:"TRANSACTION_COMPLETE", data: json.data});
                    }
                }
            )
        );
};

let undo = (smokerId, dispatch) => {
    fetch("/api/fin/undo/" + smokerId)
        .then(response =>
            response.json().then(json => {
                    iziToast.show({
                        title: json.message.title,
                        message: json.message.text,
                        position: 'topCenter',
                        color: json.message.color
                    });
                    if (json.success) {
                        dispatch({type:"TRANSACTION_UNDO", data: json.data});
                    }
                }
            )
        );
};


const mapStateToProps = state => {
    return {
        smoker: state.smokers[state.smokerId],
        smokers: state.smokers
    }
};

const mapDispatchToProps = dispatch => {
    // const contextId = Math.floor(Math.random() * 1000000);
    return {
        clickTo: (page) => { dispatch({type: "SELECT_PAGE", page: page}); },
        reward: (smoker, amount) => { transfer("reward", smoker.id, BROKER_ID, amount, "Agent reward", smoker.id, dispatch ) },
        auction: (smoker, amount) => { transfer("auction", smoker.id, BROKER_ID, amount, "Auction", smoker.id, dispatch ) },
        punish: (smoker, amount) => { transfer("punish", smoker.id, BROKER_ID, amount, "Agent punishment", smoker.id, dispatch ) },
        correctPlus: (smoker, amount) => { transfer("correctPlus", WORLD_ID, smoker.id, amount, "Correction", smoker.id, dispatch ) },
        correctMinus: (smoker, amount) => { transfer("correctMinus", smoker.id, WORLD_ID, amount, "Correction", smoker.id, dispatch ) },
        chequeFrom: (from, to, amount) => { transfer("chequeFrom", from.id, to.id, amount, "Cheque", to.id, dispatch ) },
        transferTo: (from, to, amount) => { transfer("transferTo", from.id, to.id, amount, "Cheque", from.id, dispatch ) },
        warfare: (from, to, amount) => { transfer("warfare", from.id, to.id, amount, "Warfare", from.id, dispatch ) },
        missionHandle: (from, to, amount) => { transfer("missionHandle", from.id, BROKER_ID, 0.5, "Mission handle", from.id, dispatch ) },
        infoOnSmoker: (from, to, amount) => { transfer("infoOnSmoker", from.id, to.id, 7, "Info on", from.id, dispatch ) },
        undo: (smoker) => {undo(smoker.id, dispatch) },
    }
};

const Transaction = connect( mapStateToProps, mapDispatchToProps )(TransactionUI);
export default Transaction