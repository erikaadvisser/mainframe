import React from 'react';
import { connect } from 'react-redux'
import MainframeTitle from "../../generic/MainframeTitle";
import Projects from "./Projects";

const BrokerHomeUI = ({ smokers, statements, clickSmoker, clickStatement }) => {

    const statementLabel = (statement) => {
        if (statement.start === "-") {
            return "initial";
        }
        else {
            return statement.end + " round: " + statement.round;
        }
    };

    return (
        <span>
            <MainframeTitle />
            <div className="row">
                <div className="col-lg-12">
                    <h2>
                        <span className="label label-default">Main</span>
                    </h2>
                </div>
            </div>
            <div className="row">
                <div className="col-lg-12">
                    <hr className="hr"/>
                </div>
            </div>
            <div className="row">
                <div className="col-lg-3">
                    <div className="buttonRow">
                        Smoker
                    </div>
                    { smokers
                        .filter(smoker => smoker.smoker )
                        .map(smoker =>
                            <div key={smoker.id} onClick={() => clickSmoker(smoker)}>
                                <span className='btn btn-info buttonRow button'>{smoker.name}</span>
                            </div>
                        ) }
                </div>
                <div className="col-lg-3">
                    <div className="buttonRow">
                        Statement
                    </div>
                    { statements
                        .map( statement =>
                            <div key={statement.id} onClick={() => clickStatement(statement)}>
                                <span className='btn btn-info buttonRow button'>{statementLabel(statement)}</span>
                            </div>
                        )
                    }
                </div>
            </div>
            <Projects/>
        </span>
    )
};

const mapStateToProps = state => {
    return {
        smokers: state.smokers,
        statements: state.statements
    }
};

const mapDispatchToProps = dispatch => {
    return {
        clickSmoker: smoker => { dispatch({type: "SELECT_SMOKER", smoker: smoker}); },
        clickStatement: statement => { window.open('http://carcosa:8080/api/statement/' + statement.id, "_blank"); },

    }
};

const BrokerHome = connect(
    mapStateToProps,
    mapDispatchToProps
)(BrokerHomeUI);

export default BrokerHome
