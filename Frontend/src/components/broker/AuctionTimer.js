import React from 'react';
import {connect} from "react-redux";

import TimerUI from "./generic/TimerUI"

const AuctionTimerUi = ({timer, clickShowBids, clickPause, clickPlay, clickEnd}) => {

    return (
        <span>
            <div className="row">
                <div className="col-lg-12">
                    <h1 className="title">
                        <span className="label label-default">Auction</span>
                    </h1>
                </div>
            </div>
            <div className="row">
                <div className="col-lg-12">
                    <h2 className="">
                        { TimerUI( {timer, clickShowBids, clickPause, clickPlay, clickEnd, clickForward: null} ) }
                    </h2>
                </div>
            </div>
            <div className="row">
                <div className="col-lg-12">
                    <hr className="hr"/>
                </div>
            </div>
        </span>
    );
};

const mapStateToProps = state => {
    return {
        timer: state.auctionTimer,
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        clickShowBids: () => { fetch("/api/auction/showBids"); },
        clickPause: () => { fetch("/api/auction/pause"); },
        clickPlay: () => { fetch("/api/auction/play"); },
        clickEnd: () => { fetch("/api/auction/end"); },

    }
};

const AuctionTimer = connect(mapStateToProps, mapDispatchToProps)(AuctionTimerUi);
export default AuctionTimer

