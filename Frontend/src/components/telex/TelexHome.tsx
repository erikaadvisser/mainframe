import React from 'react';
import {useSelector} from "react-redux";
import {TelexRootState} from "./TelexRoot";

export const TelexHome = () => {
    const telexes = useSelector((state: TelexRootState) => state.telexLines);

    return (
        <div className="row">
            <table className="table table-striped" id="table">
                <thead>
                <tr>
                    <th className="telexFont" style={{width: "280px"}}>Time</th>
                    <th className="telexFont">Message</th>
                    <th className="telexFont" style={{width: "200px"}}>Agents</th>
                </tr>
                </thead>
                <tbody>
                {telexes.map(telex => (
                    <tr key={telex.id}>
                        <td className="telex-time telexFont">{telex.time}</td>
                        <td className="telex-text telexFont">{telex.message}</td>
                        <td className="telex-agents telexFont">{telex.agents}</td>
                    </tr>
                ))
                }
                </tbody>
            </table>
        </div>
    )
}
