import {AnyAction} from "redux";


export interface TelexLine {
    id: string,
    time: string,
    message: string,
    agents: string,
}

export const telexLinesReducer = (state: TelexLine[] = [], action: AnyAction): TelexLine[]  => {
    switch(action.type) {
        case "INIT_STATE": return action.state.telexes
        case "SERVER_TELEX": return addTelex(state, action.data)
        default: return state
    }
}

const addTelex = (state: TelexLine[], telex: TelexLine) => {
    let newTelexes = state.slice()
    newTelexes.splice(0,0, telex)

    return newTelexes;
}
