import {combineReducers, Reducer} from "redux";
import React from "react";
import {Provider} from "react-redux";
import {configureStore} from "@reduxjs/toolkit";
import {TelexLine, telexLinesReducer} from "./TelexesReducer";
import {TelexHome} from "./TelexHome";

export const TelexRoot = () => {
    const store = configureStore({
        reducer: telexRootReducer as Reducer<TelexRootState>,
        preloadedState: {telexLines: [{id: "1", time: "2021-01-01 12:00:00", message: "Hello", agents: "Cooper trooper"}]},
        middleware: (getDefaultMiddleware) =>  [...getDefaultMiddleware()],
        devTools: true
    })

    console.log("store state");
    console.log(store.getState());

    return (<Provider store={store}>
        <TelexHome/>
    </Provider>)
}

export interface TelexRootState {
    telexLines: TelexLine[]
}

const telexRootReducer = combineReducers({
    telexLines: telexLinesReducer
})


