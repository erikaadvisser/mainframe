import React from 'react';


export const Chooser = () => {

    const navigate = (path: string) => {
        window.document.location.href = path
    }
    return (
            <div>
                <div className="row">
                    <div className="col-lg-12 text-center">
                        <br />
                        <br />
                        <h1>
                            <span className="label label-default">Choose function</span>
                        </h1>
                        <br />
                        <h1>
                            <span className="label label-info" onClick={() => navigate("/broker")}>Broker</span>
                        </h1>
                        <h1>
                            <span className="label label-info" onClick={() => navigate("/gm")}>GM</span>
                        </h1>
                        <h1>
                            <span className="label label-info" onClick={() => navigate("/telex")}>Telex</span>
                        </h1>
                        <h1>
                            <span className="label label-info" onClick={() => navigate("/agentStats")}>Agent stats</span>
                        </h1>
                        <br />
                        <br />
                        <br />
                    </div>
                </div>
            </div>
    )
}

