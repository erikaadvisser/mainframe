import React from 'react';
import {connect} from "react-redux";

const NotificationSenderUI = ({user}) => {

    return (
        <div>
            <br/>
            <form className="form-horizontal" onSubmit={ (event) => sendMessage(event, user) }>
                <input type="text" id="messageInput" className="form-control" placeholder="Message" autoComplete="off" />&nbsp;
            </form>
        </div>
    );
};

const sendMessage = (event, user )=> {
    let text = document.getElementById('messageInput').value;
    let message = { text: text };

    fetch("/api/notification/send/" + user, {
        method: 'post',
        body: JSON.stringify(message),
        headers: {
            'Content-Type': 'application/json'
        },
    } );

    event.target.reset();
    event.preventDefault();
};


const mapStateToProps = state => {
    return {
        user: state.user
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
    }
};

const NotificationSender = connect(mapStateToProps, mapDispatchToProps)(NotificationSenderUI);
export default NotificationSender
