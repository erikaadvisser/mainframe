import React from 'react';
import {connect} from "react-redux";

const mapNotification = (notification, clickProcessed) => {

    let acknowledgement = null;
    if (notification.acknowledged != null) {
        let acknowledge = notification.acknowledged ? "glyphicon-ok" : "glyphicon-send";
        let acknowledgeClasses = "glyphicon " + acknowledge;
        acknowledgement = (
            <span className="pull-right">
                <span className={acknowledgeClasses} />
            </span>
        );
    }

    let style = {"fontWeight": notification.processed ? "normal" : "bold"};

    return (
        <tr key={notification.id}>
            <td><input type="checkbox" checked={notification.processed} className="pull-right" onClick={() => clickProcessed(notification)}/></td>
            <td style={style}>{notification.time}</td>
            <td style={style}>{notification.sender}</td>
            <td style={style} className="notification-text-td" >
                <span>{notification.message}</span>
                {acknowledgement}
            </td>
        </tr>
    );
};



const NotificationsUi = ({notifications, user}) => {

    const clickProcessed = (notification) => {
        if (notification.processed) {
            fetch("/api/notification/undoProcess/" + notification.id)
        }
        else {
            fetch("/api/notification/process/" + notification.id)

        }

    };


    return (
        <span>
            <div className="row notifications">
                <table className="table table-striped" id="table">
                    <thead>
                        <tr>
                            <th></th>
                            <th>Time</th>
                            <th>From</th>
                            <th>Message</th>
                        </tr>
                    </thead>
                    <tbody>
                    {notifications
                        .filter(notification => (notification.target === user || notification.target === "All"))
                        .map(notification => (mapNotification(notification, clickProcessed)))
                    }
                    </tbody>
                </table>
            </div>
        </span>
    );
};


const mapStateToProps = state => {
    return {
        notifications: state.notifications,
        user: state.user
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
    }
};

const Notification = connect(mapStateToProps, mapDispatchToProps)(NotificationsUi);
export default Notification