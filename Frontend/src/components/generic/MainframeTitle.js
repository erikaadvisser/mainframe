import React from 'react'
import { connect } from 'react-redux'

const MainframeTitleUI = () => {
    return (
        <div className="row">
            <div className="col-lg-12">
                <h1 className="title">
                    <span className="label label-default pull-left">Mainframe</span>
                </h1>
            </div>
        </div>
    );
};

const MainframeTitle = connect()( MainframeTitleUI );
export default MainframeTitle
