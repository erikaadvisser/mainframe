import React from 'react';
import {connect, useSelector} from "react-redux";
import {Agent} from "./AgentsReducer";
import {AgentStatsRootState} from "./AgentStatsOverviewRoot";

const agentRow = (agent: Agent) => {
    return (
    <tr key={agent.id}>
        {/*<td className="agencyStats">{agent.agency}</td>*/}
        <td className="agencyStats">{agent.name}</td>
        <td className="agencyStats text-center">{agent.stats['INFILTRATION']}</td>
        <td className="agencyStats text-center">{agent.stats['PERSUASION']}</td>
        <td className="agencyStats text-center">{agent.stats['SCIENCE']}</td>
    </tr>);
};

const summaryRow = (agent1: Agent, agent2: Agent) => {
    return (
        <tr key={agent1.agency}>
            {/*<td className="agencyStats">{agent1.agency}</td>*/}
            <td className="agencyStats"><u>{agent1.agency}</u></td>
            <td className="agencyStats text-center"><u>{agent1.stats['INFILTRATION'] + agent2.stats['INFILTRATION']}</u></td>
            <td className="agencyStats text-center font-weight-bold"><u>{agent1.stats['PERSUASION'] + agent2.stats['PERSUASION']}</u></td>
            <td className="agencyStats text-center font-weight-bold"><u>{agent1.stats['SCIENCE'] + agent2.stats['SCIENCE']}</u></td>
        </tr>);
};

const emptyRow = (agent: Agent) => {
    return (
        <tr key={agent.agency + "_empty"}>
            <td className="agencyStats">&nbsp;</td>
            <td className="agencyStats">&nbsp;</td>
            <td className="agencyStats">&nbsp;</td>
            <td className="agencyStats">&nbsp;</td>
            <td className="agencyStats">&nbsp;</td>
        </tr>);
};


export const AgentStatsOverview = () => {

    const agents = useSelector((state: AgentStatsRootState) => state.agents)

    if (agents.length === 0) {
        return (<span></span>);
    }

    console.log(agents);
    const dana = findAgent(agents, 0)
    const fox = findAgent(agents, 1)
    const adrik = findAgent(agents, 2)
    const anoushka = findAgent(agents, 3)
    const dinorah = findAgent(agents, 4)
    const janko = findAgent(agents, 5)


    return (
        <span>
            <div className="row">
                <table className="table table-striped" id="table">
                    <thead>
                        <tr>
                            {/*<th className="agencyStats">Agency</th>*/}
                            <th className="agencyStats">&nbsp;</th>
                            <th className="agencyStats text-center">Infiltration</th>
                            <th className="agencyStats text-center">Persuasion</th>
                            <th className="agencyStats text-center">Science</th>
                        </tr>
                    </thead>
                    <tbody>
                    { summaryRow(dana, fox) }
                    { agentRow(dana) }
                    { agentRow(fox) }
                    { emptyRow(dana)}

                    { summaryRow(adrik, anoushka) }
                    { agentRow(adrik) }
                    { agentRow(anoushka) }
                    { emptyRow(adrik)}

                    { summaryRow(dinorah, janko) }
                    { agentRow(dinorah) }
                    { agentRow(janko) }
                    </tbody>
                </table>
            </div>
        </span>
    );
};

const findAgent = (agents: Agent[], id: number) : Agent => {
    const candidate = agents.find( it => it.id === id )
    if (candidate === undefined) {
        throw Error("Agent not found: " + id);
    }
    return candidate
}
