import {AnyAction} from "redux";

export enum AgentStat {
    SCIENCE = "SCIENCE",
    INFILTRATION = "INFILTRATION",
    PERSUASION = "PERSUASION",
}


export type AgentStatsMap = {
    [key in AgentStat]: number;
};

export interface Agent {
    id: number
    name: string
    agency: string
    stats: AgentStatsMap
}

export const agentsReducer = (state: Agent[] = [], action: AnyAction): Agent[]  => {
    switch (action.type) {
        case "INIT_STATE" :
            return action.state.agents;
        case "UPDATE_AGENT_STATS":
            return updateAgent(state, action.data);
        default           :
            return state;
    }
}

interface UpdateAgentAction {
    id: number,
    stats: AgentStatsMap
}

const updateAgent = (state: Agent[], updatedAgent: UpdateAgentAction) => {
    let agents = state.slice();

    let toModify = agents.find(it => it.id === updatedAgent.id);
    if (toModify === undefined) {
        alert("Agent not found to update stats: " + updatedAgent.id);
        throw Error("Agent not found to update stats: " + updatedAgent.id);
    }
    toModify.stats = updatedAgent.stats;

    return agents;
};

