import {combineReducers, Reducer} from "redux";
import React from "react";
import {Provider} from "react-redux";
import {configureStore} from "@reduxjs/toolkit";
import {Agent, agentsReducer} from "./AgentsReducer";
import {AgentStatsOverview} from "./AgentStatsOverview";
import {stompConnection} from "../../util/Websocket";

export const AgentStatsOverviewRoot = () => {
    const store = configureStore({
        reducer: agentStatsOverviewRootReducer as Reducer<AgentStatsRootState>,
        preloadedState: {agents: [
                {id: 0, name: "Agent 0", agency: "FBI", stats: {SCIENCE: 1, INFILTRATION: 2,PERSUASION: 3}},
                {id: 1, name: "Agent 1", agency: "FBI", stats: {SCIENCE: 1, INFILTRATION: 2,PERSUASION: 3}},
                {id: 2, name: "Agent 2", agency: "Agency 1", stats: {SCIENCE: 1, INFILTRATION: 2,PERSUASION: 3}},
                {id: 3, name: "Agent 3", agency: "Agency 1", stats: {SCIENCE: 1, INFILTRATION: 2,PERSUASION: 3}},
                {id: 4, name: "Agent 4", agency: "KGB", stats: {SCIENCE: 1, INFILTRATION: 2,PERSUASION: 3}},
                {id: 5, name: "Agent 5", agency: "KGB", stats: {SCIENCE: 1, INFILTRATION: 2,PERSUASION: 3}},
            ]},
        middleware: (getDefaultMiddleware) =>  [...getDefaultMiddleware()],
        devTools: true
    })

    console.log("store state");
    console.log(store.getState());

    stompConnection.connect(store, () => {
        fetch("/api/agents/all")
    })

    return (<Provider store={store}>
        <AgentStatsOverview />
    </Provider>)
}

export interface AgentStatsRootState {
    agents: Agent[]
}

const agentStatsOverviewRootReducer = combineReducers({
    agents: agentsReducer
})


