const agents = [
    {id: 0, name: "Dana Scully", agency: "FBI", color: "btn-primary"},
    {id: 1, name: "Fox Mulder", agency: "FBI", color: "btn-primary"},
    {id: 2, name: "Adrik Petrov", agency: "KGB", color: "btn-warning"},
    {id: 3, name: "Anouska Vasilev", agency: "KGB", color: "btn-warning"},
    {id: 4, name: "Dinorah Liss", agency: "MOSSAD", color: "btn-success"},
    {id: 5, name: "Janko Shameel", agency: "MOSSAD", color: "btn-success"},
];

export {agents}