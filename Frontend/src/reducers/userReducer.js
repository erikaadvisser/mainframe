import { updateQueryStringParameter} from "../util/urlUtils"

export default (state = null, action) => {
    switch(action.type) {
        case "SELECT_USER" : return selectUser(action.user);
        default: return state;
    }
}

const selectUser = (name) => {
    updateQueryStringParameter("user", name);
    document.title = name;
    return name;
};
