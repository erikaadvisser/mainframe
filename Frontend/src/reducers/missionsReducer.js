export default (state = [] , action) => {
    switch(action.type) {
        case "INIT_STATE" : return action.state.missions;
        case "MISSION_UPDATE": return missionComplete(state, action.missions);
        default           : return state;
    }
}

const missionComplete = (state, missions) => {
    let newMissions= state.slice();

    missions.forEach(mission => {
        let toModify = newMissions.find( it => it.id === mission.id );
        toModify.state = mission.state;
    });

    return newMissions;
};

