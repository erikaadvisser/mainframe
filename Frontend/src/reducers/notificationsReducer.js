let iziToast = require("izitoast");

export default (state = [] , action) => {
    switch(action.type) {
        case "INIT_STATE": return action.state.notifications;
        case "SERVER_NOTIFICATION": return addNotification(state, action.data, action.globalState);
        case "SERVER_NOTIFICATION_PROCESSED": return processNotification(state, action.data, action.globalState);
        default: return state;
    }
}

const addNotification = (state, notification, globalState) => {

    let fromMe = (notification.sender === globalState.user);
    let processed = fromMe;
    let acknowledged = fromMe ? false : null;
    let notiToAdd = {...notification, processed: processed, acknowledged: acknowledged};

    if (notification.target ===  globalState.user || (notification.target === "All" && globalState.user !== 'Telex')) {

        iziToast.show({
            title: notification.sender,
            message: notification.message,
            position: 'topCenter',
            color: "yellow",
            timeout: 10000,
        });

    }

    let newNotifications = state.slice();
    newNotifications.splice(0,0, notiToAdd);
    return newNotifications;
};

const processNotification = (state, data, globalState) => {

    let id = data.id;
    let processed = data.processed;

    let newNotifications = state.slice();
    let toModify = newNotifications.find( it => it.id === id );

    let fromMe = (toModify.sender === globalState.user);
    toModify.processed = processed;
    if (fromMe) {
        toModify.acknowledged = processed;
    }
    return newNotifications;
};


