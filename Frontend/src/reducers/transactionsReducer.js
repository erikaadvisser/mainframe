export default (state = [] , action) => {
    switch(action.type) {
        case "INIT_STATE" : return action.state.transactions;
        case "TRANSACTION_COMPLETE": return transactionComplete(state, action.data.transactions);
        case "TRANSACTION_UNDO": return transactionUndo(state);
        case "SERVER_INCOME_RECEIVED": return income(state, action.data.transactions);
        default: return state;
    }
}

const transactionComplete = (state, transactions) => {
    let newTransactions = state.slice();
    transactions.forEach( (transaction) => {
        newTransactions.splice(0, 0, transaction)
    } );
    return newTransactions;
};

const transactionUndo = (state) => {
    let newTransactions = state.slice();
    newTransactions.splice(0,1);
    return newTransactions;
};

const income = (state, transactions) => {
    let newTransactions = state.slice();
    transactions.forEach( (transaction) => {
        newTransactions.splice(0, 0, transaction)
    } );
    return newTransactions;
};