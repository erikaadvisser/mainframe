import {getParameterByName, updateQueryStringParameter} from "../util/urlUtils";

export default (state = "loading", action) => {
    switch(action.type) {
        case "INIT_STATE" : return fromQuery();
        case "SELECT_SMOKER" : return selectPage("transaction");
        case "SELECT_PAGE" : return selectPage(action.page);
        default: return state;
    }
}

const selectPage = (name) => {
    updateQueryStringParameter("page", name);
    return name;
};

const fromQuery = () => {
    let urlPage= getParameterByName("page", window.location.href);
    if (urlPage) {
        return urlPage;
    }
    return "home";
};