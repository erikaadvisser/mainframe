export default (state = 0 , action) => {
    switch(action.type) {
        case "SELECT_SMOKER" : return action.smoker.id;
        default: return state;
    }
}