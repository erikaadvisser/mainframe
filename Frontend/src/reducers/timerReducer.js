const ONE_SECOND = 1000;

const incomeTimerReducer = (state = null, action) => {
    if (state === null) {
        state = {
            currentMillis: 0,
            nextMillis: 0,
            paused: true,
            initialized: false
        };
    }
    switch (action.type) {
        case "TICK" : return tick(state);
        case "SERVER_INCOME_TIMER": return timing(action.data);
        default: return state;
    }
};

const auctionTimerReducer = (state = null, action) => {
    if (state === null) {
        state = {
            currentMillis: 0,
            nextMillis: 0,
            paused: true,
            initialized: false
        };
    }
    switch (action.type) {
        case "TICK" : return tick(state);
        case "SERVER_AUCTION_TIMER": return timing(action.data);
        default: return state;
    }
};

const tick = (state) => {
    if (state.paused) {
        return state
    }
    return {...state, currentMillis: state.currentMillis + ONE_SECOND}
};

const timing = (data) => {
    return {
        currentMillis: data.currentMillis,
        nextMillis: data.nextMillis,
        paused: data.paused,
        initialized: true,
    };
};




export { incomeTimerReducer, auctionTimerReducer }