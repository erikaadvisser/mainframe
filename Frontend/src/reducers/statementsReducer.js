export default (state = [] , action) => {
    switch(action.type) {
        case "INIT_STATE" : return action.state.statements.slice(0,10);
        case "SERVER_STATEMENT_RECEIVED": return received(state, action.data);
        default: return state;
    }
}

const received = (state, statement) => {
    let newStatements = state.slice(0, 9);
    newStatements.splice(0, 0, statement);
    return newStatements;
};