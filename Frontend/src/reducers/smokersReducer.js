export default (state = [] , action) => {
    switch(action.type) {
        case "INIT_STATE" : return action.state.smokers;
        case "TRANSACTION_COMPLETE": return transactionComplete(state, action.data.funds);
        case "TRANSACTION_UNDO": return transactionComplete(state, action.data.funds);
        case "SERVER_INCOME_RECEIVED": return transactionComplete(state, action.data.funds);
        default: return state;
    }
}

const transactionComplete = (state, funds) => {
    let smokers = state.slice();
    let fundsEntries = Object.entries(funds);
    fundsEntries.forEach( entry => {
        let id = entry[0];
        let newFunds = entry[1];

        let smokerStale = smokers[id];
        let smokerFresh = {...smokerStale, funds: newFunds};
        smokers[id] = smokerFresh
    });

    return smokers;
};
