package org.n1.mainframe.backend.service

import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.mockito.kotlin.mock
import java.math.BigDecimal
import org.assertj.core.api.Assertions
import kotlin.math.exp
import kotlin.random.Random

internal class AuctionServiceTest {

    val service = AuctionService(mock(), mock(), mock())



    @Test
    fun calculateDistribution() {
        testCalculation(listOf("0", "0", "0", "0", "0"), listOf(1, 1, 1, 1, 1))
        testCalculation(listOf("0.5", "0", "0", "0", "0"), listOf(5, 0, 0, 0, 0))
        testCalculation(listOf("5", "0", "0", "0", "0"), listOf(5, 0, 0, 0, 0))
        testCalculation(listOf("1", "1", "1", "1", "1"), listOf(1, 1, 1, 1, 1))
        testCalculation(listOf("2", "1", "1", "1", "0"), listOf(2, 1, 1, 1, 0))
        testCalculation(listOf("0", "0", "3", "1", "1"), listOf(0, 0, 3, 1, 1))
        testCalculation(listOf("20", "1", "0", "0", "0"), listOf(5, 0, 0, 0, 0)) // 4 normal, 1 highest bidder
        testCalculation(listOf("10", "1", "0", "0", "0"), listOf(5, 0, 0, 0, 0)) // 4 normal, 1 highest bidder
        testCalculation(listOf("6","6","1","0","0"), listOf(2,2,1,0,0))
    }
    @Test
    fun calculateDistribution2() {
        testCalculation(listOf("9","1","0","0","0"), listOf(4,1,0,0,0)) // 4 normal, 1 to smoker with least intel
        testCalculation(listOf("9","1","0.5","0","0"), listOf(4,1,0,0,0)) // 4 normal, 1 to smoker with least intel
        testCalculation(listOf("9","1","1","0","0"), listOf(4,1,0,0,0), listOf(4,0,1,0,0)) // 4 normal, 1 to smoker with least intel
    }

    @Test
    fun calculateDistribution3() {
        testCalculation(listOf("3","3","0","0","0"), listOf(2,3,0,0,0), listOf(3,2,0,0,0))
    }

    @Test
    fun calculateDistribution4() {
        testCalculation(listOf("6","6","5","0","0"), listOf(2,2,1,0,0))
        testCalculation(listOf("6","6","5","5","0"), listOf(2,1,1,1,0), listOf(1,2,1,1,0))
    }

    private fun testCalculation(bids: List<String>, expected: List<Int>, otherExpected: List<Int>? = null) {
        (1..100).forEach {
            testCalculationOnce(bids, expected, otherExpected)
        }
    }

    private fun testCalculationOnce(bids: List<String>, expected: List<Int>, otherExpected: List<Int>? = null) {
        service.auctionBids.clear()
        var smokerId = 0
        bids.forEach {
            val bid = BigDecimal(it)
            service.auctionBids[smokerId] = bid
            smokerId++
        }
        val distribution = service.calculateDistribution()

        val received:List<Int> = (0..4).map { smokerId -> distribution[smokerId]!! }

        if (otherExpected == null) {
            assertThat(received).containsExactlyElementsOf(expected)
        }
        else {
            try {
                assertThat(received).containsExactlyElementsOf(expected)
            }
            catch(throwable: Throwable ){
                assertThat(received).containsExactlyElementsOf(otherExpected)
            }
        }
    }
}