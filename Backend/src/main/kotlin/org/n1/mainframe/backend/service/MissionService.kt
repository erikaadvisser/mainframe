package org.n1.mainframe.backend.service

import org.n1.mainframe.backend.model.core.Mission
import org.n1.mainframe.backend.model.core.MissionState
import org.n1.mainframe.backend.model.core.MissionState.*
import org.n1.mainframe.backend.model.ui.Message
import org.n1.mainframe.backend.model.ui.Response
import org.n1.mainframe.backend.model.ui.SuccessResponse
import org.n1.mainframe.backend.repo.AgentRepo
import org.n1.mainframe.backend.repo.MissionRepo
import org.n1.mainframe.backend.repo.SmokerRepo
import org.springframework.stereotype.Service
import java.util.*
import java.util.Arrays.asList

@Service
class MissionService(val repo: MissionRepo,
                     val agentRepo: AgentRepo,
                     val smokerRepo: SmokerRepo,
                     val telexService: TelexService,
                     val notificationService: NotificationService) {

    val random = Random()

    fun start(id: Int): Mission {
        val mission = repo.get(id)
        mission.state = ACTIVE
        Thread {
            val sleepSeconds = 100L + random.nextInt(50)
            Thread.sleep(1000 * sleepSeconds)
            telexService.missionStart(mission)
        }.start()
        val notificationText = "Mission started, provide handle of ${mission.name}"
        notificationService.toBroker(notificationText, "Board")

        return mission
    }

    fun getMission(id: Int): Mission {
        return repo.get(id)
    }


    fun success(id: Int, agentIds: List<Int>): Response {
        val mission = repo.get(id)
        telexService.missionSuccess(mission, agentIds)

        val potentialNextMission = processNext(mission)

        mission.state = PENDING

        val message = Message("Ok", "${mission.name} proccessed as success")
        return createResponse(message, mission, potentialNextMission)
    }

    private fun processNext(mission: Mission): Mission? {
        if (mission.next != null) {
            return start(mission.next.id)
        }
        return null
    }


    fun expose(id: Int, agentIds: List<Int>): Response {
        val mission = repo.get(id)
        telexService.missionExpose(mission, agentIds)

        val potentialNextMission = processNext(mission)
        mission.state = COMPLETED

        val text = "Critical information revealed increasing global panic"
        telexService.send(text, agentIds)

        val notificationText = "Start auction for ${mission.name}"
        notificationService.toBroker(notificationText, "Board")

        val message = Message("Ok", "${mission.name} processed as exposed")
        return createResponse(message, mission, potentialNextMission)
    }

    fun exposeFoiled(id: Int, agentIds: List<Int>): Response {
        val mission = repo.get(id)

        success(id, agentIds)
//        val potentialNextMission = processNext(mission)
//        dont' call  processNext, this is already done in success.


        mission.state = COMPLETED
        val telexText = "Attempted exposure of critical information (failed)"
        telexService.send(telexText, agentIds)

        val notificationText = "Start auction for ${mission.name}"
        notificationService.toBroker(notificationText, "Board")

        val message = Message("Ok", "${mission.name} processed as foiled exposed")
        return createResponse(message, mission)
    }


    fun getAllWithoutNext(): List<Mission> {
        val list = repo.all.values.map { Mission(it.id, it.name, it.type, it.state) }
        list.sortedBy { it.id }
        return list
    }

    fun embezzle(id: Int, agentIds: List<Int>, smokerId: Int): Response {
        val mission = repo.get(id)
        mission.state = COMPLETED
        val smoker = smokerRepo.getSmoker(smokerId)
        val agent = agentRepo.get(agentIds[0])
        val notificationText = "${mission.name} embezzled for ${smoker.name} by ${agent.name}"
        notificationService.toBroker(notificationText, "Agents")

        val message = Message("Ok", "${mission.name} processed as embezzled")
        return createResponse(message, mission)
    }

    fun file(id: Int, agentIds: List<Int>): Response {
        val mission = repo.get(id)
        mission.state = COMPLETED
        val agent = agentRepo.get(agentIds[0])
        val notificationText = "${mission.name} filed by ${agent.name} - start Auction"
        notificationService.toBroker(notificationText, "Agents")

        val message =  Message("Ok", "${mission.name} processed as filed")
        return createResponse(message, mission)
    }

    private fun createResponse(message: Message, vararg missionData: Mission?): Response {
        val missions = missionData.filter { it != null }
        return SuccessResponse(message, missions)
    }
}