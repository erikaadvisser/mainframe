package org.n1.mainframe.backend.service

import org.springframework.stereotype.Service
import java.math.BigDecimal
import javax.annotation.PostConstruct

@Service
class InitService(val transactionService: TransactionService,
                  val statementService: StatementService,
                  val notificationService: NotificationService,
                  val smokerService: SmokerService,
                  val missionService: MissionService,
                  val telexService: TelexService) {

    @PostConstruct
    fun initialize() {
        val world = smokerService.getWorld()
        smokerService.getSmokers().forEach { smoker ->
            transactionService.transfer(world, smoker, smoker.definition.startingMoney, "Initial funds", -1)
        }
        statementService.next()

        notificationService.send("Mainframe startup", "-", "All")


        telexService.missionStart(missionService.getMission(0))
        telexService.missionStart(missionService.getMission(1))
        telexService.missionStart(missionService.getMission(2))
    }
}