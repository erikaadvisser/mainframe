package org.n1.mainframe.backend.repo

import org.apache.coyote.http11.Constants.a
import org.n1.mainframe.backend.model.core.Statement
import org.springframework.stereotype.Repository
import java.util.concurrent.atomic.AtomicInteger

@Repository
class StatementRepo {

    val statements = HashMap<Int, Statement>()
    val nextId = AtomicInteger(0)

    var currentStatement: Statement? = null

    fun create(start: String, end: String): Statement {
        val id = nextId.getAndIncrement()
        val statement = Statement(id, id, start, end)
        statements[id] = statement

        currentStatement = statement
        return statement
    }

    fun getCurrent(): Statement? {
        return currentStatement
    }

    fun getAll(): List<Statement> {
        val list = ArrayList(statements.values)
        list.sortedBy { it.id }
        return list
    }


}