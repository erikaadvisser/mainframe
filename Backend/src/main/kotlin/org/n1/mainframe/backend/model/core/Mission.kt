package org.n1.mainframe.backend.model.core

import org.n1.mainframe.backend.model.core.MissionState.INACTIVE

class Mission(val id: Int,
              val name: String,
              val type: String,
              var state: MissionState = INACTIVE,
              val next: Mission? = null)

enum class MissionState {
    INACTIVE, ACTIVE, PENDING, COMPLETED
}