package org.n1.mainframe.backend.service

import org.n1.mainframe.backend.model.core.Smoker
import org.n1.mainframe.backend.model.core.Timing
import org.springframework.stereotype.Service
import java.math.BigDecimal
import java.util.stream.IntStream.range

@Service
class AuctionService(val stompService: StompService,
                     val smokerService: SmokerService,
                     val notificationService: NotificationService) {

    private val FIVE_MINUTES = 1000 * 60 * 5

    var auctionStarted = false;
    var paused = true
    var millisToEnd = FIVE_MINUTES

    val auctionBids = HashMap<Int, BigDecimal>().withDefault { BigDecimal.ZERO }.toMutableMap()

    init {
        Thread { tick() }.start()
    }

    fun tick() {
        while(true) {
            Thread.sleep(1000)
            if (paused) {
                continue
            }
            millisToEnd -= 1000
            if (millisToEnd <= 0) {
                paused = true
                millisToEnd = 0
                sendTiming()
            }
        }
    }

    fun auctionStart() {
        auctionBids.clear()
        (0..4).forEach { auctionBids[it] = BigDecimal.ZERO.setScale(1) }
        notificationService.toBroker("Started auction timer", "Broker");
        auctionStarted = true;
    }

    /** additional bids are added to existing bigs */
    fun bid(smoker: Smoker, bid: BigDecimal) {
        auctionBids.merge(smoker.id, bid) { existing: BigDecimal, change: BigDecimal -> existing + change }
    }

    fun end() {
        val now = System.currentTimeMillis()
        val timing = Timing(now, now + millisToEnd, paused)

        stompService.toBroker("SERVER_AUCTION_END", timing)

        val evidenceDistribution = distribute()
        notificationService.toBroker(evidenceDistribution, "Mainframe")
        auctionStarted = false;

        millisToEnd = FIVE_MINUTES
        paused = true
        sendTiming()
    }

    fun sendTiming() {
        val now = System.currentTimeMillis()
        val timing = Timing(now, now + millisToEnd, paused)
        stompService.toBroker("SERVER_AUCTION_TIMER", timing)
    }


    fun pause() {
        this.paused = true
        sendTiming()
    }

    fun play() {
        this.paused = false
        if (!auctionStarted) {
            auctionStart()
        }
        sendTiming()
    }

    fun showBids() {
        val bidString = auctionBids.map { entry ->
            val smoker = smokerService.getSmoker(entry.key)
            "$smoker: ${entry.value} grip"
        }.joinToString(", ")
        notificationService.toBroker(bidString, "Mainframe")
    }

    private fun distribute(): String {
        val distribution = calculateDistribution()

        return distribution.map {entry ->
            val smoker = smokerService.getSmoker(entry.key)
            "$smoker: ${entry.value}"
        } .joinToString(", ")
    }

    fun calculateDistribution(): Map<Int, Int> {
        auctionBids.keys.forEach { smokerId -> auctionBids.computeIfPresent(smokerId) { _, value -> value.setScale(2) } }
        return calculateDistribution(mapOf( 0 to 0, 1 to 0, 2 to 0, 3 to 0, 4 to 0).toMutableMap())
    }

    private fun calculateDistribution(intelDistribution: MutableMap<Int, Int>): Map<Int, Int> {
        val intelToDistribute: Int = 5 - intelDistribution.values.sum()

        val totalBid = auctionBids.values.sumOf { it }
        if (totalBid.compareTo( BigDecimal.ZERO) == 0) {
            return divideAmongstSmokersWhoBid(intelDistribution)
        }

        val priceInitialIntel = totalBid.setScale(2) / BigDecimal(intelToDistribute)


        auctionBids.keys.forEach { smokerId ->
            var bid = auctionBids[smokerId]!!
            while (bid >= priceInitialIntel) {
                auctionBids.merge(smokerId, priceInitialIntel) { existing: BigDecimal, payment: BigDecimal -> existing - payment }
                takeOneIntel(smokerId, intelDistribution)
                bid = auctionBids[smokerId]!!
            }
        }

        val intelLeft= 5 - intelDistribution.values.sum()
        if (intelLeft == 0) { return intelDistribution }
        if (intelLeft ==1) { return highestBidder(intelDistribution) }

        if (intelLeft == intelToDistribute) { // deadlock
            val distributionFixed = highestBidder(intelDistribution)
            return calculateDistribution(distributionFixed)
        }

        return calculateDistribution(intelDistribution)
    }


    private fun divideAmongstSmokersWhoBid(intelDistribution: MutableMap<Int, Int>): MutableMap<Int, Int> {
        var contenderSmokerIds = intelDistribution.map{ entry: Map.Entry<Int, Int> ->
            if (entry.value > 0) { entry.key } else { null }
        }.filterNotNull()

        if (contenderSmokerIds.isEmpty()) {
            contenderSmokerIds = listOf( 0,1,2,3,4)
        }


        val shuffledList = contenderSmokerIds.shuffled()
        val divideList = (shuffledList + shuffledList + shuffledList + shuffledList).toMutableList()

        val intelToDistribute: Int = 5 - intelDistribution.values.sum()
        (1..intelToDistribute).forEach {
            val receiverId = divideList.removeAt(0)
            takeOneIntel(receiverId, intelDistribution)
        }
        return intelDistribution
    }


    private fun highestBidder(intelDistribution: MutableMap<Int, Int>): MutableMap<Int, Int> {
        val highestBidLeft = auctionBids.maxWithOrNull { x, y -> x.value.compareTo(y.value) }!!.value

        val highestBidders = auctionBids.filter { entry -> entry.value == highestBidLeft}.keys
        val highestBiddersSorted = highestBidders.sortedBy { x -> intelDistribution[x] }

        val winnerId = highestBiddersSorted.first()
        takeOneIntel(winnerId, intelDistribution)
        auctionBids[winnerId] = BigDecimal.ZERO

        return intelDistribution
    }

    private fun takeOneIntel(smokerId: Int, intelDistribution: MutableMap<Int, Int>) {
        intelDistribution.merge(smokerId, 1) { existing: Int, added: Int -> existing + added }
    }


}