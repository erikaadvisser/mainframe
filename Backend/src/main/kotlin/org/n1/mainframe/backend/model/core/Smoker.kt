package org.n1.mainframe.backend.model.core

import java.math.BigDecimal
import java.util.*

data class Smoker (val definition: SmokerDefinition) {
    var funds = BigDecimal.ZERO
    val id = definition.id
    val name = definition.fullName
    val transactions = LinkedList<Transaction>()
    val smoker = definition.smoker

    override fun toString(): String {
        return name
    }
}