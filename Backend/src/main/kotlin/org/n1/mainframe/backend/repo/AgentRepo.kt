package org.n1.mainframe.backend.repo

import org.n1.mainframe.backend.model.core.Agency
import org.n1.mainframe.backend.model.core.Agent
import org.n1.mainframe.backend.model.core.AgentStat
import org.springframework.stereotype.Repository

@Repository
class AgentRepo {

    val all = HashMap<Int, Agent>()

    init {
        val scully = Agent(0, "Dana Scully", Agency.FBI, mapOf(AgentStat.SCIENCE to 3, AgentStat.INFILTRATION to 2, AgentStat.PERSUASION to 1).toMutableMap())
        val mulder = Agent(1, "Fox Mulder", Agency.FBI, mapOf(AgentStat.SCIENCE to 2, AgentStat.INFILTRATION to 1, AgentStat.PERSUASION to 3).toMutableMap())
        val adrik = Agent(2, "Adrik Petrov", Agency.KGB, mapOf(AgentStat.SCIENCE to 1, AgentStat.INFILTRATION to 3, AgentStat.PERSUASION to 2).toMutableMap())
        val anouska = Agent(3, "Anouska Vasilev", Agency.KGB, mapOf(AgentStat.SCIENCE to 3, AgentStat.INFILTRATION to 2, AgentStat.PERSUASION to 1).toMutableMap())
        val dinorah = Agent(4, "Dinorah Liss", Agency.MOSSAD, mapOf(AgentStat.SCIENCE to 1, AgentStat.INFILTRATION to 3, AgentStat.PERSUASION to 2).toMutableMap())
        val janko = Agent(5, "Janko Shameel", Agency.MOSSAD, mapOf(AgentStat.SCIENCE to 2, AgentStat.INFILTRATION to 1, AgentStat.PERSUASION to 3).toMutableMap())

        all[mulder.id] = mulder
        all[scully.id] = scully
        all[adrik.id] = adrik
        all[anouska.id] = anouska
        all[dinorah.id] = dinorah
        all[janko.id] = janko

    }

    fun get(id: Int): Agent {
        return all[id] !!
    }

}
