package org.n1.mainframe.backend.service

import org.n1.mainframe.backend.model.core.Statement
import org.n1.mainframe.backend.repo.StatementRepo
import org.n1.mainframe.backend.util.formattedTime
import org.springframework.stereotype.Service

@Service
class StatementService(val repo: StatementRepo,
                       val stomp: StompService) {

    fun getNextId(): Int {
        val current = repo.getCurrent()
        return ( current?.id ?: -1 ) + 1
    }

    fun next() {
        val current = repo.getCurrent()

        val start = current?.end ?: "-"
        val end = formattedTime()
        val statement = repo.create(start, end)

        stomp.toBroker("SERVER_STATEMENT_RECEIVED", statement)
    }

    fun initial() {
        val start = "-"
        val end = formattedTime()
        repo.create(start, end)
    }

    fun getAll(): List<Statement> {
        return repo.getAll().reversed()
    }

    fun get(id: Int): Statement {
        return repo.statements[id] !!
    }

}

