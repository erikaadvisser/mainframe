package org.n1.mainframe.backend.web

import org.n1.mainframe.backend.model.ui.Message
import org.n1.mainframe.backend.model.ui.Response
import org.n1.mainframe.backend.model.ui.SuccessResponse
import org.n1.mainframe.backend.service.MissionService
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("/api/mission/")
class MissionController(val missionService: MissionService) {

    class MissionParameters(val id: Int = 0, val agentIds: List<Int> = ArrayList(), val smokerId: Int? = null)

    @PostMapping("/success")
    fun success(@RequestBody params: MissionParameters): Response {
        return missionService.success(params.id, params.agentIds)
    }

    @PostMapping("/expose")
    fun expose(@RequestBody params: MissionParameters): Response {
        return missionService.expose(params.id, params.agentIds)
    }

    @PostMapping("/expose_foiled")
    fun exposeFoiled(@RequestBody params: MissionParameters): Response {
        return missionService.exposeFoiled(params.id, params.agentIds)
    }

    @PostMapping("/embezzle")
    fun embezzle(@RequestBody params: MissionParameters): Response {
        return missionService.embezzle(params.id, params.agentIds, params.smokerId!!)
    }

    @PostMapping("/file")
    fun file(@RequestBody params: MissionParameters): Response {
        return missionService.file(params.id, params.agentIds)
    }

    @GetMapping("/startProject/{missionId}")
    fun startProject(@PathVariable("missionId") missionId: Int): Response {
        val mission = missionService.start(missionId)
        return SuccessResponse(Message("Ok", "Project started, mission ${mission.name} initiated."), mission)
    }

}