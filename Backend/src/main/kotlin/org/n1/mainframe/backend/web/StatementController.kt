package org.n1.mainframe.backend.web

import org.n1.mainframe.backend.model.core.Smoker
import org.n1.mainframe.backend.model.core.Transaction
import org.n1.mainframe.backend.service.SmokerService
import org.n1.mainframe.backend.service.StatementService
import org.n1.mainframe.backend.service.TransactionService
import org.springframework.stereotype.Controller
import org.springframework.ui.Model
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestMapping
import java.math.BigDecimal

@Controller
class StatementController(val statementService: StatementService,
                          val transactionService: TransactionService,
                          val smokerService: SmokerService) {

    var lastStatement = -1

    @RequestMapping("/api/statement/{id}")
    fun transfer(@PathVariable("id") id: Int,
                 model: Model): String {
        model.addAttribute("statement", statementService.get(id))


        val allSmokers = smokerService.smokerMap()
        val statement = smokerService.getSmokers().map { smoker ->
            val transactions = transactionService.getForStatement(smoker, id).map { toUITransaction(smoker, it, allSmokers) }
            UIStatement(smoker, transactions)
        }

        model.addAttribute("smokersStatements", statement)
        model.addAttribute("close", lastStatement != id)

        lastStatement = id
        return "statement"
    }

    class UIStatement(val smoker: Smoker, val transactions: List<UITransaction>)

    class UITransaction(val time: String, val direction: String, val amount: BigDecimal, val description: String);

    fun toUITransaction(smoker: Smoker, transaction: Transaction, smokers: Map<Int, Smoker>): UITransaction {
        val direction: String
        val amount: BigDecimal

        if (transaction.to == smoker.id) {
            val from = smokers[transaction.from] !!
            direction = "<- ${from.name}"
            amount = transaction.amount.setScale(1)
        } else {
            val to = smokers[transaction.to] !!
            direction = "-> ${to.name}"
            amount = transaction.amount.setScale(1).negate()
        }

        return UITransaction(transaction.getTimeString(), direction, amount, transaction.description)
    }

}