package org.n1.mainframe.backend.service

import org.n1.mainframe.backend.model.core.Smoker
import org.n1.mainframe.backend.model.core.SmokerDefinition
import org.n1.mainframe.backend.model.core.Transaction
import org.n1.mainframe.backend.model.ui.Message
import org.n1.mainframe.backend.model.ui.SuccessResponse
import org.n1.mainframe.backend.repo.SmokerRepo
import org.n1.mainframe.backend.repo.TransactionRepo
import org.springframework.stereotype.Service
import java.math.BigDecimal
import java.math.RoundingMode

enum class TransferAction {
    reward,
    auction,
    punish,
    correctPlus,
    correctMinus,
    chequeFrom,
    warfare,
    missionHandle,
    infoOnSmoker,
}

val actionsAllowedWhileBroke = listOf(TransferAction.chequeFrom, TransferAction.correctPlus, TransferAction.correctMinus)

@Service
class TransactionService(
    val transactionRepo: TransactionRepo,
    val statementService: StatementService,
    val smokerService: SmokerService,
    val auctionService: AuctionService,
    val notificationService: NotificationService
) {

    fun getAll(): List<Transaction> {
        val transactions = ArrayList(transactionRepo.transactions.values)
        transactions.sortBy { it.time }
        transactions.reverse()
        return transactions
    }

    fun transferAction(action: TransferAction, from: Int, to: Int, amount: BigDecimal, description: String, context: Int): TransactionResponse {
        val payer = smokerService.getSmoker(from)
        val receiver = smokerService.getSmoker(to)
        val world = smokerService.getSmoker(SmokerDefinition.WORLD.id)

        val balanceAfterTransaction = payer.funds - amount;

        if (balanceAfterTransaction < BigDecimal.ZERO && !actionsAllowedWhileBroke.contains(action)) {
            error("Not enough money for this");
        }

        when (action) {
            TransferAction.warfare -> return attack(payer, receiver, amount, description, context)
            TransferAction.chequeFrom -> {
                val transferTransactions = transfer(payer, receiver, amount, description, context)
                val transactions = listOf(transferTransactions).toMutableList()

                if (balanceAfterTransaction < BigDecimal.ZERO) {
                    val costsTransaction = transfer(payer, world, BigDecimal("0.5"), "Negative balance costs ", -1)
                    transactions.add(costsTransaction);
                }

                return TransactionResponse(payer, receiver, transactions)
            }
            TransferAction.infoOnSmoker -> {
                Thread {
                    val sleepSeconds = 60 * 15
                    Thread.sleep((1000 * sleepSeconds).toLong())
                    notificationService.send("Give ${payer} filing cabinet info on ${receiver}", "-", "Broker")
                }.start()

                val transaction = transfer(payer, world, amount, "Info on ${receiver}", -1)
                return TransactionResponse(payer, receiver, listOf(transaction))
            }
            TransferAction.auction -> {
                auctionService.bid(payer, amount)
                val transaction = transfer(payer, receiver, amount, description, context)
                return TransactionResponse(payer, receiver, listOf(transaction))
            }
            else -> {
                val transaction = transfer(payer, receiver, amount, description, context)
                return TransactionResponse(payer, receiver, listOf(transaction))
            }
        }
    }

    private fun attack(attacker: Smoker, victim: Smoker, amount: BigDecimal, description: String, context: Int): TransactionResponse {
        val damage = (amount).setScale(0, RoundingMode.FLOOR).setScale(1) / BigDecimal(2)
        val world = smokerService.getSmoker(SmokerDefinition.WORLD.id)

        val attackTransaction = transfer(attacker, world, amount, "Attack ${victim}, damage: ${damage} grip.", context)
        val victimTransaction = transfer(victim, world, damage, "Attacked by ${attacker.name}", SmokerDefinition.WORLD.id)

        return TransactionResponse(attacker, victim, listOf(attackTransaction, victimTransaction))
    }

    fun transfer(payer: Smoker, receiver: Smoker, amount: BigDecimal, description: String, context: Int, action: TransferAction? = null): Transaction {
        payer.funds -= amount
        receiver.funds += amount
        smokerService.save(payer)
        smokerService.save(receiver)
        val statementId = statementService.getNextId()
        return transactionRepo.create(payer.id, receiver.id, amount, description, context, statementId)
    }

    fun undo(smokerId: Int): TransactionResponse {
        val transactions = getAll()
        if (transactions.isEmpty()) {
            error("No transactions in system")
        }
        val transaction = transactions.first()

        if (transaction.context != smokerId) {
            val smoker = smokerService.getSmoker(transaction.context)
            error("Last tansaction belongs to ${smoker.name}.")
        }

        val currentStatementId = statementService.getNextId()
        if (transaction.statementId != currentStatementId) {
            error("This transaction is already part of a statement, use a correction instead.")
        }

        transactionRepo.remove(transaction)
        val payer = smokerService.getSmoker(transaction.from)
        val receiver = smokerService.getSmoker(transaction.to)
        payer.funds += transaction.amount
        receiver.funds -= transaction.amount
        smokerService.save(payer)
        smokerService.save(receiver)

        return TransactionResponse(payer, receiver, listOf(transaction))
    }

    fun getForStatement(smoker: Smoker, statementId: Int): List<Transaction> {
        return getAll()
            .filter {
                (it.from == smoker.id || it.to == smoker.id) && (it.statementId == statementId)
            }
    }
}

class TransactionResponse(
    smoker1: Smoker, smoker2: Smoker,
    val transactions: List<Transaction>
) {

    val funds = HashMap<Int, BigDecimal>()

    init {
        funds[smoker1.id] = smoker1.funds
        funds[smoker2.id] = smoker2.funds
    }
}
