package org.n1.mainframe.backend.model.core

class Timing(val currentMillis: Long,
             val nextMillis: Long,
             val paused: Boolean)