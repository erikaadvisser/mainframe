package org.n1.mainframe.backend.web

import org.n1.mainframe.backend.model.core.SmokerDefinition.WORLD
import org.n1.mainframe.backend.model.ui.FailureResponse
import org.n1.mainframe.backend.model.ui.Message
import org.n1.mainframe.backend.model.ui.Response
import org.n1.mainframe.backend.model.ui.SuccessResponse
import org.n1.mainframe.backend.service.SmokerService
import org.n1.mainframe.backend.service.TransactionService
import org.n1.mainframe.backend.service.TransferAction
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import java.math.BigDecimal
import java.math.BigDecimal.ZERO
import java.math.RoundingMode

@RestController
@RequestMapping("/api/fin")
class FinanceController(val smokerService: SmokerService,
                        val transactionService: TransactionService) {

    @GetMapping("/transfer/{action}/{from}/{to}/{amount}/{description}/{context}")
    fun transfer(@PathVariable("action") action: TransferAction,
                 @PathVariable("from") from: Int,
                 @PathVariable("to") to: Int,
                 @PathVariable("amount") amount: BigDecimal,
                 @PathVariable("description") description: String,
                 @PathVariable("context") context: Int): Response {
        try {
            if (amount <= ZERO) {
                return FailureResponse("Failure", "Please enter a positive amount")
            }
            val stateChange = transactionService.transferAction(action, from, to, amount, description, context)

            return SuccessResponse(Message("Ok", "Transfer approved"), stateChange)
        } catch (exception: Exception) {
            return FailureResponse("Failure", exception)
        }
    }


    @GetMapping("/undo/{id}")
    fun undo(@PathVariable("id") id: Int): Response {
        try {
            val stateChange = transactionService.undo(id)
            return SuccessResponse(Message("Ok", "Rolled back transaction \"${stateChange.transactions[0].description}\""), stateChange)
        } catch (exception: Exception) {
            return FailureResponse("Failure", exception)
        }
    }

}