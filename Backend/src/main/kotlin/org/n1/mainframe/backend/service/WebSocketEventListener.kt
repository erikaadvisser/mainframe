import org.springframework.context.event.EventListener
import org.springframework.stereotype.Service
import org.springframework.web.socket.messaging.SessionConnectedEvent


@Service
class WebSocketEventListener {

//    @Autowired
//    private val messagingTemplate: SimpMessageSendingOperations? = null

    @EventListener
    fun handleWebSocketConnectListener(event: SessionConnectedEvent) {
        println("Received a new web socket connection")
    }

//    @EventListener
//    fun handleWebSocketDisconnectListener(event: SessionDisconnectEvent) {
//        val headerAccessor = StompHeaderAccessor.wrap(event.message)
//
//        val username = headerAccessor.sessionAttributes["username"] as String
//        if (username != null) {
//            logger.info("User Disconnected : " + username)
//
//            val chatMessage = ChatMessage()
//            chatMessage.setType(ChatMessage.MessageType.LEAVE)
//            chatMessage.setSender(username)
//
//            messagingTemplate!!.convertAndSend("/topic/public", chatMessage)
//        }
//    }
}