package org.n1.mainframe.backend.service

import org.n1.mainframe.backend.model.core.Agent
import org.n1.mainframe.backend.model.core.Mission
import org.n1.mainframe.backend.model.core.Telex
import org.n1.mainframe.backend.model.ui.Message
import org.n1.mainframe.backend.repo.AgentRepo
import org.n1.mainframe.backend.repo.TelexRepo
import org.springframework.stereotype.Service
import java.util.*

@Service
class TelexService(val repo: TelexRepo,
                   val agentRepo: AgentRepo,
                   val stompService: StompService) {

    private val messages = Properties()

    init {
        val propertiesInput = ClassLoader.getSystemResource("telex.properties").openStream()
        messages.load(propertiesInput)
    }

    fun missionStart(mission: Mission) {
        sendKey("${mission.type}.start")
    }

    fun missionSuccess(mission: Mission, agentIds: List<Int>) {
        sendKey("${mission.type}.success", agentIds)
    }

    fun missionExpose(mission: Mission, agentIds: List<Int>) {
        sendKey("${mission.type}.expose", agentIds)
    }


    private fun sendKey(key: String, agentIds: List<Int>? = null) {
        var text = messages.getProperty(key).trim()
        if (text.isBlank()) {
           text = "(telex message unreadable, partial text: ${key})"
        }
        send (text, agentIds)
    }

    fun send(text: String, agentIds: List<Int>? = null ) {
        val agentText = createAgentString(agentIds)
        val telex = repo.create(text, agentText)
        val message = Message("Telex", text)
        stompService.toBroker("SERVER_TELEX", telex, message)
    }

    fun getAll():List<Telex> {
        return repo.all.reversed()
    }

    private fun createAgentString(agentIds: List<Int>?): String {
        if (agentIds == null) {
            return ""
        }
        val agents = agentIds.map { id -> agentRepo.get(id) }
        val agentString = agents.fold("", { total, agent -> "$total${agent.name}, " })
        return agentString.trimEnd(',', ' ')
    }
}