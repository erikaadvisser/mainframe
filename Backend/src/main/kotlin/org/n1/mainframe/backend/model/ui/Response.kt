package org.n1.mainframe.backend.model.ui

import java.awt.SystemColor.text

open class Response(val success: Boolean, val message: Message, val data: Any? = null)

class SuccessResponse(message: Message, data: Any? = null) : Response(true, message, data)

class FailureResponse (message: Message) : Response(false, message, null) {
    constructor(title: String, text: String) : this(Message(title, text, "red"))
    constructor(title: String, exception: Exception) : this(Message(title, exception.message ?: "Server problem: ${exception.javaClass.simpleName}", "red"))
}
