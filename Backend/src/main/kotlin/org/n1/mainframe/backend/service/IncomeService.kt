package org.n1.mainframe.backend.service

import org.n1.mainframe.backend.model.core.Timing
import org.n1.mainframe.backend.model.core.Transaction
import org.springframework.stereotype.Service
import java.math.BigDecimal


@Service
class IncomeService(
    val smokerService: SmokerService,
    val transactionService: TransactionService,
    val notificationService: NotificationService,
    val statementService: StatementService,
    val stomp: StompService
) {

    private val FIFTEEN_MINUTES = 1000 * 60 * 15

    var millisToNext = FIFTEEN_MINUTES
    var paused = true
    var round = 0

    init {
        Thread { tick() }.start()
    }

    fun tick() {
        while (true) {
            Thread.sleep(1000)
            if (paused) {
                continue
            }
            millisToNext -= 1000
            if (millisToNext <= 0) {
                income()
            }
        }
    }

    fun sendTiming() {
        val now = System.currentTimeMillis()
        val timing = Timing(now, now + millisToNext, paused)
        stomp.toBroker("SERVER_INCOME_TIMER", timing)
    }

    fun income() {
        val smokers = smokerService.getSmokers()
        val world = smokerService.getWorld()

        val transactions = ArrayList<Transaction>()
        smokers.forEach { smoker ->
            val transaction = transactionService.transfer(world, smoker, BigDecimal("2.5"), "Income", world.id)
            transactions.add(transaction)
        }

        val funds = HashMap<Int, BigDecimal>()
        smokers.forEach { smoker ->
            funds[smoker.id] = smoker.funds
        }
        round++

        val data = IncomeResponse(transactions, funds)
        stomp.toBroker("SERVER_INCOME_RECEIVED", data)
        notificationService.toBroker("Income round $round", "Mainframe")

        millisToNext = FIFTEEN_MINUTES
        sendTiming()
        statementService.next()
    }

    fun pause() {
        this.paused = true
        sendTiming()
    }

    fun play() {
        this.paused = false
        sendTiming()
    }
}

data class IncomeResponse(
    val transactions: List<Transaction>,
    val funds: Map<Int, BigDecimal>
)