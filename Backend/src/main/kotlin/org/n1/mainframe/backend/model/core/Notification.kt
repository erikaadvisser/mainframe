package org.n1.mainframe.backend.model.core

import java.time.LocalTime

class Notification(val id: Int,
                   val message: String,
                   val sender: String,
                   val target: String,
                   var processed: Boolean = false) {

    private val time = LocalTime.now()

    fun getTime(): String {
        return "%02d:%02d".format(time.hour, time.minute)
    }

}