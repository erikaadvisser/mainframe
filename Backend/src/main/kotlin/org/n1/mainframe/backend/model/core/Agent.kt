package org.n1.mainframe.backend.model.core

class Agent(val id: Int,
            val name: String,
            val agency: Agency,
            val stats: MutableMap<AgentStat, Int>)