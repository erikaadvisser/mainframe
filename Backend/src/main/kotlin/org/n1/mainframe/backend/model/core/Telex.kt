package org.n1.mainframe.backend.model.core

import org.n1.mainframe.backend.util.formattedTime
import java.time.LocalTime

class Telex(val id: Int,
            val text: String,
            val agents: String? = "",
            val time: String = formattedTime())