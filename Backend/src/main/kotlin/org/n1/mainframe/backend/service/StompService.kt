package org.n1.mainframe.backend.service

import com.fasterxml.jackson.databind.ObjectMapper
import org.n1.mainframe.backend.model.ui.Message
import org.n1.mainframe.backend.model.ui.ReduxEvent
import org.n1.mainframe.backend.util.formattedTime
import org.springframework.messaging.simp.SimpMessageSendingOperations
import org.springframework.stereotype.Service
import java.io.FileWriter


@Service
class StompService(val stompTemplate: SimpMessageSendingOperations) {

    private val writer = FileWriter("log.txt")

    private val objectMapper = ObjectMapper()


    fun toBroker(action: String, data: Any? = null, message: Message? = null) {
        val event = ReduxEvent(action, data, message)
        stompTemplate.convertAndSend("/topic/broker", event)

        log(action, message, data)
    }

    private fun log(action: String, message: Message?, data: Any?) {

        var logMessage = "${formattedTime()} - "

        if (message != null) {
            val messageText = "${message.title}:${message.text} "
        }
        if (data != null) {
            val dataText = objectMapper.writeValueAsString(data)
            logMessage += ": " + dataText
        }
        logMessage += "\n"
        writer.append(logMessage)
        writer.flush()
    }
}