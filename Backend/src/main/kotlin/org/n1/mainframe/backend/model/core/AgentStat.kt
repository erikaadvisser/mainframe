package org.n1.mainframe.backend.model.core

enum class AgentStat {
    SCIENCE,
    INFILTRATION,
    PERSUASION
}