package org.n1.mainframe.backend.web

import org.n1.mainframe.backend.model.ui.Message
import org.n1.mainframe.backend.model.ui.Response
import org.n1.mainframe.backend.model.ui.SuccessResponse
import org.n1.mainframe.backend.service.IncomeService
import org.springframework.messaging.simp.SimpMessageSendingOperations
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/api/income/")
class IncomeController(val incomeService: IncomeService,
                       val stompTemplate: SimpMessageSendingOperations) {

    @GetMapping("/do")
    fun doit(): Response {
        incomeService.income()
        return SuccessResponse(Message())
    }

    @GetMapping("/getTimer")
    fun getTiming(): Response {
        incomeService.sendTiming()
        return SuccessResponse(Message())
    }

    @GetMapping("/pause")
    fun pause(): Response {
        incomeService.pause()
        return SuccessResponse(Message())
    }

    @GetMapping("/play")
    fun play(): Response {
        incomeService.play()
        return SuccessResponse(Message())
    }
}