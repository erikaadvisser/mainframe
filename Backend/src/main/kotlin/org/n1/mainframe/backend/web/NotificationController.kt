package org.n1.mainframe.backend.web

import org.n1.mainframe.backend.model.ui.Message
import org.n1.mainframe.backend.model.ui.Response
import org.n1.mainframe.backend.model.ui.SuccessResponse
import org.n1.mainframe.backend.model.ui.UiNotification
import org.n1.mainframe.backend.service.NotificationService
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("/api/notification/")
class NotificationController(val notificationService: NotificationService) {


    @PostMapping("/send/{user}")
    fun send(@PathVariable("user")user: String,
            @RequestBody uiNotification: UiNotification): Response {
        notificationService.send(uiNotification.text, user, "All")
        return SuccessResponse(Message())
    }

    @GetMapping("/process/{id}")
    fun process(@PathVariable("id")id: Int) {
        notificationService.process(id, true)
    }

    @GetMapping("/undoProcess/{id}")
    fun undoProcess(@PathVariable("id")id: Int) {
        notificationService.process(id, false)
    }

}