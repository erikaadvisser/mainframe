package org.n1.mainframe.backend.web

import org.n1.mainframe.backend.model.core.AgentStat
import org.n1.mainframe.backend.model.ui.Response
import org.n1.mainframe.backend.service.AgentService
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/api/agent/")
class AgentController(val agentService: AgentService) {


    data class AgentStatsChange(
        val id: Int,
        val stat: AgentStat,
        val delta: Int
    )

    @GetMapping("all")
    fun sendAll() {
        agentService.sendAll()

    }

    @PostMapping("stats")
    fun success(@RequestBody message: AgentStatsChange) {
        agentService.changeStat(message.id, message.stat, message.delta)
    }
}