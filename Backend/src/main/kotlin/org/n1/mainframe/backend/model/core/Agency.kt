package org.n1.mainframe.backend.model.core

enum class Agency {
    FBI, KGB, MOSSAD
}