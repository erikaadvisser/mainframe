package org.n1.mainframe.backend.repo

import org.n1.mainframe.backend.model.core.Telex
import org.springframework.stereotype.Repository
import java.awt.SystemColor.text
import java.util.concurrent.atomic.AtomicInteger

@Repository
class TelexRepo {

    val all = ArrayList<Telex>()
    val nextId = AtomicInteger(0)

    init {
        create("Telex printer active")
    }

    fun create(text: String, agents: String? = null): Telex {
        val telex = Telex(nextId.getAndIncrement(), text, agents)
        all.add(telex)

        return telex
    }

}