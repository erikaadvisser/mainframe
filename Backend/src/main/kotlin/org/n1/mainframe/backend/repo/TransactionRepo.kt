package org.n1.mainframe.backend.repo;

import org.n1.mainframe.backend.model.core.Statement
import org.n1.mainframe.backend.model.core.Transaction
import org.springframework.stereotype.Repository;
import java.math.BigDecimal
import java.util.concurrent.atomic.AtomicInteger

@Repository
class TransactionRepo {

    val transactions = HashMap<Int, Transaction>()
    val nextId = AtomicInteger(0)

    fun create(from: Int, to: Int, amount: BigDecimal, description: String, context: Int, statementId: Int):Transaction {
        val id = nextId.getAndIncrement()
        val transaction = Transaction(id, amount, from, to, description, context, statementId)
        transactions[id] = transaction
        return transaction
    }

    fun remove(toRemove: Transaction) {
        transactions.remove(toRemove.id)
    }


}
