package org.n1.mainframe.backend.service

import mu.KLogging
import org.springframework.messaging.simp.stomp.StompHeaderAccessor
import org.springframework.web.socket.messaging.SessionConnectEvent
import org.springframework.context.ApplicationListener
import org.springframework.stereotype.Component


@Component
class StompConnectEvent(
        val incomeService: IncomeService,
        val auctionService: AuctionService) : ApplicationListener<SessionConnectEvent> {

    companion object: KLogging()

    override fun onApplicationEvent(event: SessionConnectEvent) {

        val sha = StompHeaderAccessor.wrap(event.message)
        logger.debug { "Connect event [sessionId: ${sha.sessionId}]" }
    }
}