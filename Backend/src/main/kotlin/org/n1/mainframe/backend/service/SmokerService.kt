package org.n1.mainframe.backend.service

import org.n1.mainframe.backend.model.core.Smoker
import org.n1.mainframe.backend.model.core.SmokerDefinition.*
import org.n1.mainframe.backend.repo.SmokerRepo
import org.springframework.stereotype.Service
import java.util.*

@Service
class SmokerService(
        val smokerRepo: SmokerRepo ) {

    fun getSmoker(id: Int): Smoker {
        return smokerRepo.getSmoker(id)
    }

    fun getAll(): List<Smoker> {
        return smokerRepo.all
    }

    fun getSmokers():List<Smoker> {
        return smokerRepo.all.filter { it.smoker }
    }

    fun getWorld(): Smoker {
        return smokerRepo.all.find{ it.definition == WORLD } !!
    }

    fun smokerMap(): Map<Int, Smoker> {
        return smokerRepo.smokerMap
    }

    fun save(smoker: Smoker) {
        smokerRepo.save(smoker)
    }

}