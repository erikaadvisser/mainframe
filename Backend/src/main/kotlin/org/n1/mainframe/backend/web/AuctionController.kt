package org.n1.mainframe.backend.web

import org.n1.mainframe.backend.model.ui.Message
import org.n1.mainframe.backend.model.ui.Response
import org.n1.mainframe.backend.model.ui.SuccessResponse
import org.n1.mainframe.backend.service.AuctionService
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/api/auction/")
class AuctionController(val auctionService: AuctionService) {

    @GetMapping("/getTimer")
    fun getTimer(): Response {
        auctionService.sendTiming()
        return SuccessResponse(Message())
    }

    @GetMapping("/play")
    fun play(): Response {
        auctionService.play()
        return SuccessResponse(Message())
    }

    @GetMapping("/pause")
    fun pause(): Response {
        auctionService.pause()
        return SuccessResponse(Message())
    }

    @GetMapping("/showBids")
    fun showBids(): Response {
        auctionService.showBids()
        return SuccessResponse(Message())
    }

    @GetMapping("/end")
    fun end(): Response {
        auctionService.end()
        return SuccessResponse(Message())
    }

}