package org.n1.mainframe.backend.web

import org.n1.mainframe.backend.model.core.Smoker
import org.n1.mainframe.backend.service.*
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/api")
class MainController(
    val agentService: AgentService,
    val smokerService: SmokerService,
    val transactionService: TransactionService,
    val statementService: StatementService,
    val missionService: MissionService,
    val notificationService: NotificationService,
    val telexService: TelexService
) {

    @GetMapping("/state")
    fun state(): Map<String, Any> {
        val smokers = smokerService.getAll()
        val transactions = transactionService.getAll()
        val statements = statementService.getAll()
        val missions = missionService.getAllWithoutNext()
        val notifications = notificationService.getAll()
        val telexes = telexService.getAll()
        val agents = agentService.getAll()

        return hashMapOf(
            "smokers" to smokers,
            "transactions" to transactions,
            "statements" to statements,
            "missions" to missions,
            "notifications" to notifications,
            "telexes" to telexes,
            "agents" to agents
        )
    }

    @GetMapping("/smoker/{id}")
    fun smokerById(@PathVariable("id") id: Int): Smoker {
        return smokerService.getSmoker(id)
    }


}