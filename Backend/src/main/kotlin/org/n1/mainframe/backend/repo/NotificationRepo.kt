package org.n1.mainframe.backend.repo

import org.n1.mainframe.backend.model.core.Notification
import org.springframework.stereotype.Repository
import java.util.concurrent.atomic.AtomicInteger

@Repository
class NotificationRepo {

    val nextId = AtomicInteger(0)
    val all = HashMap<Int, Notification>()

    fun create(messageText: String, sender: String, target: String):Notification {
        val id = nextId.getAndIncrement()
        val notification = Notification(id, messageText, sender, target)
        all[id] = notification
        return notification
    }

    fun get(id: Int): Notification {
        return all[id]!!
    }

}