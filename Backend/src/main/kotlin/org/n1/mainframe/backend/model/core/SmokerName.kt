package org.n1.mainframe.backend.model.core

import java.math.BigDecimal

enum class SmokerDefinition(val id:Int, val fullName:String, val smoker: Boolean, val startingMoney: BigDecimal) {

    ITALIAN(0,"The Italian", true, BigDecimal(4)),
    WONG(1,"Mr. Wong", true, BigDecimal(4)),
    SCIENTIST(2,"The Scientist", true, BigDecimal(4)),
    DIMITRI(3,"Dimitri", true, BigDecimal(15)),
    TECHNOCRAT(4,"The Technocrat", true, BigDecimal(9)),
    BROKER(5,"The Broker", false, BigDecimal(0)),
    WORLD(6, "other", false, BigDecimal(0))
}