package org.n1.mainframe.backend.model.core

import java.math.BigDecimal
import java.time.LocalTime

data class Transaction(
    val id: Int,
    val amount: BigDecimal,
    val from: Int, // id of the sender
    val to: Int, // id of the receiver
    val description: String,


    // the id of the smoker whose context the transaction happens in.
    // Used for undo transactions to make sure you don't undo transactions of other smokers.
    val context: Int,

    val statementId: Int
) {
    val time = LocalTime.now()

    fun getTimeString(): String {
        return "%02d:%02d".format(time.hour, time.minute)
    }
}