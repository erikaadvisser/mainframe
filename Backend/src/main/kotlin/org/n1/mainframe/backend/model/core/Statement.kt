package org.n1.mainframe.backend.model.core

class Statement(val id: Int,
                val round: Int,
                val start: String,
                val end: String)