package org.n1.mainframe.backend.service

import org.n1.mainframe.backend.model.core.Agent
import org.n1.mainframe.backend.model.core.AgentStat
import org.n1.mainframe.backend.repo.AgentRepo
import org.springframework.stereotype.Service

@Service

class AgentService(
    val agentRepo: AgentRepo,
    val stompService: StompService
) {

    fun getAll(): List<Agent> {
        return agentRepo.all.values.sortedBy { it.name }.sortedBy { it.agency }
    }


    data class UpdateAgentStatsMessage(
        val id: Int,
        val stats: Map<AgentStat, Int>
    )

    fun changeStat(id: Int, stat: AgentStat, delta: Int) {
        val agent = agentRepo.get(id);
        agent.stats[stat] = agent.stats[stat]!! + delta

        val action = UpdateAgentStatsMessage(id, agent.stats)
        stompService.toBroker("UPDATE_AGENT_STATS", action)
    }

    fun sendAll() {
        val agents = getAll()
        stompService.toBroker("ALL_AGENTS", agents)
    }
 }