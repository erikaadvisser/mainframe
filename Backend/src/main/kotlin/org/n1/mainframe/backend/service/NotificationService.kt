package org.n1.mainframe.backend.service

import org.n1.mainframe.backend.model.core.Notification
import org.n1.mainframe.backend.model.ui.Message
import org.n1.mainframe.backend.repo.NotificationRepo
import org.springframework.stereotype.Service
import java.util.concurrent.atomic.AtomicInteger

@Service
class NotificationService(val repo: NotificationRepo,
                          val stomp: StompService) {

    fun toBroker(messageText: String, sender: String) {
        send(messageText, sender, "Broker")
    }

    fun send(messageText: String, sender: String, target: String) {
        val notification = repo.create(messageText, sender, target)
        stomp.toBroker("SERVER_NOTIFICATION", notification)
    }

    class ProcessMessage(val id: Int, val processed: Boolean)
    fun process(id: Int, processed: Boolean) {
        val notification = repo.get(id)
        notification.processed = processed
        stomp.toBroker("SERVER_NOTIFICATION_PROCESSED", ProcessMessage(id, processed))
    }

    fun getAll():List<Notification> {
        val list = ArrayList(repo.all.values)
        list.sortBy { it.id }
        return list.reversed()
    }
}