package org.n1.mainframe.backend.repo

import org.n1.mainframe.backend.model.core.Smoker
import org.n1.mainframe.backend.model.core.SmokerDefinition
import org.springframework.stereotype.Repository
import java.util.ArrayList

/**
 *
 */
@Repository
class SmokerRepo {
    val smokers = ArrayList<Smoker>()
    val all = ArrayList<Smoker>()
    val smokerMap = HashMap<Int, Smoker>()

    init {
        smokers.add(Smoker(SmokerDefinition.ITALIAN))
        smokers.add(Smoker(SmokerDefinition.WONG))
        smokers.add(Smoker(SmokerDefinition.SCIENTIST))
        smokers.add(Smoker(SmokerDefinition.DIMITRI))
        smokers.add(Smoker(SmokerDefinition.TECHNOCRAT))
        all.addAll(smokers)
        all.add(Smoker(SmokerDefinition.BROKER))
        all.add(Smoker(SmokerDefinition.WORLD))

        all.forEach { smokerMap[it.id] = it }
    }

    fun getSmoker(id: Int): Smoker {
        return all.find { it.id == id } !!
    }

    fun find(id: Int): Smoker {
        return all[id]
    }

    fun save(payer: Smoker) {
        // do nothing yet, preparation for actual persistence
    }

}