package org.n1.mainframe.backend.repo

import org.n1.mainframe.backend.model.core.Mission
import org.n1.mainframe.backend.model.core.MissionState
import org.springframework.stereotype.Repository

@Repository
class MissionRepo {

    val all = HashMap<Int, Mission>()

    init {

        val italian_2_1 = Mission(6, "Italian 2-1", "italian_2_1")
        val wong_2_1 = Mission(7, "Wong 2-1", "wong_2_1")
        val scientist_2_1 = Mission(8, "Scientist 2-1", "scientist_2_1")
        val dimitri_2_1 = Mission (9, "Dimitri 2-1", "dimitri_2_1")
        val technocrat_2_1 = Mission (10, "Technocrat 2-1", "technocrat_2_1")

        val italian_1_2 = Mission(3, "Italian 1-2", "italian_1_2")
        val wong_1_2 = Mission(4, "Wong 1-2", "wong_1_2")
        val scientist_1_2 = Mission(5, "Scientist 1-2", "scientist_1_2")

        val italian_1_1 = Mission(0, "Italian 1-1", "italian_1_1", MissionState.ACTIVE, italian_1_2)
        val wong_1_1 = Mission(1, "Wong 1-1", "wong_1_1", MissionState.ACTIVE, wong_1_2)
        val scientist_1_1 = Mission(2, "Scientist 1-1", "scientist_1_1", MissionState.ACTIVE, scientist_1_2)



        all[italian_1_1.id] = italian_1_1
        all[wong_1_1.id] = wong_1_1
        all[scientist_1_1.id] = scientist_1_1

        all[scientist_1_2.id] = scientist_1_2
        all[italian_1_2.id] = italian_1_2
        all[wong_1_2.id] = wong_1_2

        all[italian_2_1.id] = italian_2_1
        all[wong_2_1.id] = wong_2_1
        all[scientist_2_1.id] = scientist_2_1
        all[dimitri_2_1.id] = dimitri_2_1
        all[technocrat_2_1.id] = technocrat_2_1

    }

    fun get(id: Int): Mission {
        return all[id]!!
    }
}